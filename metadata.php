<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// Metadata version.

use OxidEsales\Eshop\Application\Controller\Admin\ArticleSeo as OxArticleSeo;
use OxidEsales\Eshop\Application\Controller\Admin\CategorySeo as OxCategorySeo;
use OxidEsales\Eshop\Application\Controller\Admin\ContentSeo as OxContentSeo;
use OxidEsales\Eshop\Application\Controller\Admin\ManufacturerSeo as OxManufacturerSeo;
use OxidEsales\Eshop\Application\Controller\Admin\ModuleConfiguration as OxModuleConfiguration;
use OxidEsales\Eshop\Application\Controller\Admin\VendorSeo as OxVendorSeo;
use OxidEsales\Eshop\Application\Controller\ArticleDetailsController as OxArticleDetailsController;
use OxidEsales\Eshop\Application\Controller\ArticleListController as OxArticleListController;
use OxidEsales\Eshop\Application\Controller\ContentController as OxContentController;
use OxidEsales\Eshop\Application\Controller\ManufacturerListController as OxManufacturerListController;
use OxidEsales\Eshop\Application\Controller\VendorListController as OxVendorListController;
use OxidEsales\Eshop\Application\Model\Article as OxArticle;
use OxidEsales\Eshop\Application\Model\Category as OxCategory;
use OxidEsales\Eshop\Application\Model\Content as OxContent;
use OxidEsales\Eshop\Application\Model\Manufacturer as OxManufacturer;
use OxidEsales\Eshop\Application\Model\Vendor as OxVendor;
use TheRealWorld\SeoModule\Application\Controller\Admin\ArticleSeo;
use TheRealWorld\SeoModule\Application\Controller\Admin\CategorySeo;
use TheRealWorld\SeoModule\Application\Controller\Admin\ContentSeo;
use TheRealWorld\SeoModule\Application\Controller\Admin\ManufacturerSeo;
use TheRealWorld\SeoModule\Application\Controller\Admin\ModuleConfiguration;
use TheRealWorld\SeoModule\Application\Controller\Admin\VendorSeo;
use TheRealWorld\SeoModule\Application\Controller\ArticleDetailsController;
use TheRealWorld\SeoModule\Application\Controller\ArticleListController;
use TheRealWorld\SeoModule\Application\Controller\ContentController;
use TheRealWorld\SeoModule\Application\Controller\ManufacturerListController;
use TheRealWorld\SeoModule\Application\Controller\SeoSitemapGenerateData;
use TheRealWorld\SeoModule\Application\Controller\VendorListController;
use TheRealWorld\SeoModule\Application\Model\Article;
use TheRealWorld\SeoModule\Application\Model\Category;
use TheRealWorld\SeoModule\Application\Model\Content;
use TheRealWorld\SeoModule\Application\Model\Manufacturer;
use TheRealWorld\SeoModule\Application\Model\Vendor;
use TheRealWorld\ToolsPlugin\Core\ToolsModuleVersion;

$sMetadataVersion = '2.1';

/**
 * Module information.
 */
$aModule = [
    'id'    => 'trwseo',
    'title' => [
        'de' => 'the-real-world - SEO (sitemap.xml, meta-title)',
        'en' => 'the-real-world - SEO (sitemap.xml, meta-title)',
    ],
    'description' => [
        'de' => 'Erzeugt eine sitemap.xml für Artikel, Kategorien, Texte. Ermöglicht den Title-Tag zu setzen.',
        'en' => 'Creates a sitemap.xml for products, categories, texts. Allows to set the title tag.',
    ],
    'thumbnail' => 'picture.png',
    'version'   => ToolsModuleVersion::getModuleVersion('trwseo'),
    'author'    => 'Mario Lorenz',
    'url'       => 'https://www.the-real-world.de',
    'email'     => 'mario_lorenz@the-real-world.de',
    'events'    => [
        'onActivate'   => '\TheRealWorld\SeoModule\Core\SeoEvents::onActivate',
        'onDeactivate' => '\TheRealWorld\SeoModule\Core\SeoEvents::onDeactivate',
    ],
    'extend' => [
        // Controller
        OxArticleDetailsController::class   => ArticleDetailsController::class,
        OxArticleListController::class      => ArticleListController::class,
        OxContentController::class          => ContentController::class,
        OxManufacturerListController::class => ManufacturerListController::class,
        OxVendorListController::class       => VendorListController::class,
        // Controller - Admin
        OxArticleSeo::class          => ArticleSeo::class,
        OxCategorySeo::class         => CategorySeo::class,
        OxContentSeo::class          => ContentSeo::class,
        OxVendorSeo::class           => VendorSeo::class,
        OxManufacturerSeo::class     => ManufacturerSeo::class,
        OxModuleConfiguration::class => ModuleConfiguration::class,
        // Models
        OxArticle::class      => Article::class,
        OxCategory::class     => Category::class,
        OxContent::class      => Content::class,
        OxManufacturer::class => Manufacturer::class,
        OxVendor::class       => Vendor::class,
    ],
    'controllers' => [
        'SeoSitemapGenerateData' => SeoSitemapGenerateData::class,
    ],
    'blocks' => [
        [
            'template' => 'module_config.tpl',
            'block'    => 'admin_module_config_var_type_arr',
            'file'     => 'Application/views/blocks/admin_module_config_var_type_arr.tpl',
        ],
        [
            'template' => 'object_seo.tpl',
            'block'    => 'admin_object_seo_form',
            'file'     => 'Application/views/blocks/admin_object_seo_form.tpl',
        ],
        [
            'template' => 'layout/base.tpl',
            'block'    => 'head_meta_robots',
            'file'     => 'Application/views/blocks/layout/head_meta_robots.tpl',
        ],
    ],
    'settings' => [
        [
            'group'       => 'trwseoarticle',
            'name'        => 'strTRWSeoChangeFreqoxarticle',
            'type'        => 'select',
            'value'       => 'daily',
            'constraints' => 'always|hourly|daily|weekly|monthly|yearly|never',
        ],
        [
            'group'       => 'trwseoarticle',
            'name'        => 'strTRWSeoPriorityoxarticle',
            'type'        => 'select',
            'value'       => '0.5',
            'constraints' => '0.0|0.1|0.2|0.3|0.4|0.5|0.6|0.7|0.8|0.9|1.0',
        ],
        [
            'group' => 'trwseoarticle',
            'name'  => 'boolTRWSeoActiveoxarticle',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwseoarticle',
            'name'  => 'strTRWSeoSQLConditionoxarticle',
            'type'  => 'str',
            'value' => '',
        ],
        [
            'group'       => 'trwseocategory',
            'name'        => 'strTRWSeoChangeFreqoxcategory',
            'type'        => 'select',
            'value'       => 'daily',
            'constraints' => 'always|hourly|daily|weekly|monthly|yearly|never',
        ],
        [
            'group'       => 'trwseocategory',
            'name'        => 'strTRWSeoPriorityoxcategory',
            'type'        => 'select',
            'value'       => '0.5',
            'constraints' => '0.0|0.1|0.2|0.3|0.4|0.5|0.6|0.7|0.8|0.9|1.0',
        ],
        [
            'group' => 'trwseocategory',
            'name'  => 'boolTRWSeoActiveoxcategory',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwseocategory',
            'name'  => 'strTRWSeoSQLConditionoxcategory',
            'type'  => 'str',
            'value' => '',
        ],
        [
            'group' => 'trwseocategory',
            'name'  => 'boolTRWSeoCatPageLinks',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group'       => 'trwseocontent',
            'name'        => 'strTRWSeoChangeFreqoxcontent',
            'type'        => 'select',
            'value'       => 'daily',
            'constraints' => 'always|hourly|daily|weekly|monthly|yearly|never',
        ],
        [
            'group'       => 'trwseocontent',
            'name'        => 'strTRWSeoPriorityoxcontent',
            'type'        => 'select',
            'value'       => '0.5',
            'constraints' => '0.0|0.1|0.2|0.3|0.4|0.5|0.6|0.7|0.8|0.9|1.0',
        ],
        [
            'group' => 'trwseocontent',
            'name'  => 'boolTRWSeoActiveoxcontent',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwseocontent',
            'name'  => 'strTRWSeoSQLConditionoxcontent',
            'type'  => 'str',
            'value' => '',
        ],
        [
            'group'       => 'trwseovendor',
            'name'        => 'strTRWSeoChangeFreqoxvendor',
            'type'        => 'select',
            'value'       => 'daily',
            'constraints' => 'always|hourly|daily|weekly|monthly|yearly|never',
        ],
        [
            'group'       => 'trwseovendor',
            'name'        => 'strTRWSeoPriorityoxvendor',
            'type'        => 'select',
            'value'       => '0.5',
            'constraints' => '0.0|0.1|0.2|0.3|0.4|0.5|0.6|0.7|0.8|0.9|1.0',
        ],
        [
            'group' => 'trwseovendor',
            'name'  => 'boolTRWSeoActiveoxvendor',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwseovendor',
            'name'  => 'strTRWSeoSQLConditionoxvendor',
            'type'  => 'str',
            'value' => '',
        ],
        [
            'group'       => 'trwseomanufacturer',
            'name'        => 'strTRWSeoChangeFreqoxmanufacturer',
            'type'        => 'select',
            'value'       => 'daily',
            'constraints' => 'always|hourly|daily|weekly|monthly|yearly|never',
        ],
        [
            'group'       => 'trwseomanufacturer',
            'name'        => 'strTRWSeoPriorityoxmanufacturer',
            'type'        => 'select',
            'value'       => '0.5',
            'constraints' => '0.0|0.1|0.2|0.3|0.4|0.5|0.6|0.7|0.8|0.9|1.0',
        ],
        [
            'group' => 'trwseomanufacturer',
            'name'  => 'boolTRWSeoActiveoxmanufacturer',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwseomanufacturer',
            'name'  => 'strTRWSeoSQLConditionoxmanufacturer',
            'type'  => 'str',
            'value' => '',
        ],
        [
            'group' => 'trwseoadditional',
            'name'  => 'arrTRWSeoLanguages',
            'type'  => 'arr',
            'value' => [],
        ],
        [
            'group' => 'trwseoadditional',
            'name'  => 'boolTRWSeoImages',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwseoexport',
            'name'  => 'numTRWSeoExportNrofLines',
            'type'  => 'num',
            'value' => 10,
        ],
        [
            'group' => 'trwseoexport',
            'name'  => 'boolTRWSeoCompressExport',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwmetadefault',
            'name'  => 'aarrTRWSeoMetaCategoryTitle',
            'type'  => 'aarr',
            'value' => [
                'de' => 'Beste Angebote für %title',
                'en' => 'Best deals for %title',
            ],
        ],
        [
            'group' => 'trwmetadefault',
            'name'  => 'aarrTRWSeoMetaCategoryDesc',
            'type'  => 'aarr',
            'value' => [
                'de' => 'Beste Angebote für %title finden Sie nur in unserem Onlineshop.',
                'en' => 'Best offers for %title can only be found in our online shop.',
            ],
        ],
        [
            'group' => 'trwmetadefault',
            'name'  => 'aarrTRWSeoMetaArticleTitle',
            'type'  => 'aarr',
            'value' => [
                'de' => '%title nur hier!',
                'en' => '%title only here!',
            ],
        ],
        [
            'group' => 'trwmetadefault',
            'name'  => 'aarrTRWSeoMetaArticleDesc',
            'type'  => 'aarr',
            'value' => [
                'de' => '%title und andere Dinge finden Sie nur bei uns.',
                'en' => '%title and other things can only be found with us.',
            ],
        ],
        [
            'group' => 'trwmetadefault',
            'name'  => 'aarrTRWSeoMetaArticleKeywords',
            'type'  => 'aarr',
            'value' => [
                'de' => '%keywords',
                'en' => '%keywords',
            ],
        ],
        [
            'group' => 'trwmetadefault',
            'name'  => 'aarrTRWSeoMetaManufacturerTitle',
            'type'  => 'aarr',
            'value' => [
                'de' => '%title Alle Produkte!',
                'en' => '%title all products!',
            ],
        ],
        [
            'group' => 'trwmetadefault',
            'name'  => 'aarrTRWSeoMetaManufacturerDesc',
            'type'  => 'aarr',
            'value' => [
                'de' => '%title Alle Produkte exclusiv bei uns!',
                'en' => '%title All products exclusively from us!',
            ],
        ],
        [
            'group' => 'trwmetadefault',
            'name'  => 'aarrTRWSeoMetaManufacturerKeywords',
            'type'  => 'aarr',
            'value' => [
                'de' => '',
                'en' => '',
            ],
        ],
        [
            'group' => 'trwmetadefault',
            'name'  => 'aarrTRWSeoMetaVendorTitle',
            'type'  => 'aarr',
            'value' => [
                'de' => '%title Alle Produkte!',
                'en' => '%title all products!',
            ],
        ],
        [
            'group' => 'trwmetadefault',
            'name'  => 'aarrTRWSeoMetaVendorDesc',
            'type'  => 'aarr',
            'value' => [
                'de' => '%title Alle Produkte exclusiv bei uns!',
                'en' => '%title All products exclusively from us!',
            ],
        ],
        [
            'group' => 'trwmetadefault',
            'name'  => 'aarrTRWSeoMetaVendorKeywords',
            'type'  => 'aarr',
            'value' => [
                'de' => '',
                'en' => '',
            ],
        ],
        [
            'group' => 'trwmetadefault',
            'name'  => 'aarrTRWSeoMetaContentTitle',
            'type'  => 'aarr',
            'value' => [
                'de' => '%title !',
                'en' => '%title !',
            ],
        ],
        [
            'group' => 'trwmetadefault',
            'name'  => 'aarrTRWSeoMetaContentDesc',
            'type'  => 'aarr',
            'value' => [
                'de' => '%title !',
                'en' => '%title !',
            ],
        ],
        [
            'group' => 'trwmetadefault',
            'name'  => 'aarrTRWSeoMetaContentKeywords',
            'type'  => 'aarr',
            'value' => [
                'de' => '',
                'en' => '',
            ],
        ],
        [
            'group' => 'trwmetaupdate',
            'name'  => 'boolTRWSeoMetaDeleteBeforeUpdate',
            'type'  => 'bool',
            'value' => false,
        ],
        // These options are invisible because the "group" option is null
        [
            'group' => null,
            'name'  => 'numTRWSeoFileNumber',
            'type'  => 'num',
            'value' => 1,
        ],
        [
            'group' => null,
            'name'  => 'arrTRWSeoAdditionalSeoModels',
            'type'  => 'arr',
            'value' => [],
        ],
        [
            'group' => null,
            'name'  => 'arrTRWSeoAdditionalObjectsWithImages',
            'type'  => 'arr',
            'value' => [],
        ],
    ],
];
