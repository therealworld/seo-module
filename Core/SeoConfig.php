<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Core;

use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;

class SeoConfig
{
    /** Sitemap Filename */
    protected static string $_sSiteMapFileName = 'sitemap%s.xml';

    /** Sitemap Index-Filename */
    protected static string $_sSiteMapIndexFileName = 'sitemap.xml';

    /** Sitemap Filepath, relative to root */
    protected static string $_sSiteMapFilePath = '';

    /** Sitemap Max Entries */
    protected static int $_iSitemapMaxEntries = 50000;

    /**
     * Sitemap Max FileSize in Byte
     * The max file size is 10 MB (see https://www.sitemaps.org/)
     * We took a little buffer.
     */
    protected static int $_iSitemapMaxFileSize = 9800000;

    /** StartNode Attributes */
    protected static array $_aStartNodeAttributes = [
        'xmlns'       => 'https://www.sitemaps.org/schemas/sitemap/0.9',
        'xmlns:xhtml' => 'https://www.w3.org/1999/xhtml',
    ];

    /** StartNode Attributes for Images */
    protected static array $_aStartNodeAttributesForImages = [
        'xmlns:image' => 'https://www.google.com/schemas/sitemap-image/1.1',
    ];

    /** supported Seo Models */
    protected static ?array $_aSeoModels = null;

    /** supported default Seo Models */
    protected static array $_aDefaultSeoModels = [
        'oxarticles',
        'oxcategories',
        'oxcontents',
        'oxvendor',
        'oxmanufacturers',
    ];

    /** Objects with Images */
    protected static ?array $_aObjectsWithImages = null;

    /** default Objects with Images */
    protected static array $_aDefaultObjectsWithImages = [
        'oxarticles',
        'oxcategories',
        'oxmanufacturers',
        'oxvendor',
    ];

    /** get SEO Models Types */
    public static function getSeoModels(): array
    {
        if (is_null(self::$_aSeoModels)) {
            self::$_aSeoModels = self::$_aDefaultSeoModels;
            if ($aAdditionalSeoModels = Registry::getConfig()->getConfigParam('arrTRWSeoAdditionalSeoModels')) {
                self::$_aSeoModels = array_merge(self::$_aSeoModels, $aAdditionalSeoModels);
            }
        }

        return self::$_aSeoModels;
    }

    /** get Objects With Images */
    public static function getObjectsWithImages(): array
    {
        if (is_null(self::$_aObjectsWithImages)) {
            self::$_aObjectsWithImages = self::$_aDefaultObjectsWithImages;
            if (
                $aAdditionalObjectsWithImages = Registry::getConfig()->getConfigParam(
                    'arrTRWSeoAdditionalObjectsWithImages'
                )
            ) {
                self::$_aObjectsWithImages = array_merge(self::$_aObjectsWithImages, $aAdditionalObjectsWithImages);
            }
        }

        return self::$_aObjectsWithImages;
    }

    /**
     * get the Name of the sitemap-File.
     *
     * @param bool $bNext       - get the next file in the list...
     * @param bool $bReset      - reset the filecounter
     * @param ?int $iFileNumber - optional FileNumber
     */
    public static function getSitemapFileName(
        bool $bNext = false,
        bool $bReset = false,
        ?int $iFileNumber = null
    ): string {
        if (is_null($iFileNumber)) {
            $iFileNumber = self::getSitemapFileNumber();
        }
        if ($bReset) {
            $iFileNumber = 0;
        }
        if ($bNext) {
            $iFileNumber++;
        }
        if ($bReset || $bNext) {
            ToolsConfig::saveConfigParam('numTRWSeoFileNumber', $iFileNumber, 'num', 'trwseo');
        }

        return sprintf(self::$_sSiteMapFileName, $iFileNumber);
    }

    /** get the Name of the sitemapindex-File */
    public static function getSitemapIndexFileName(): string
    {
        return self::$_sSiteMapIndexFileName;
    }

    /** get the Start Node Attributes */
    public static function getStartNodeAttributes(): array
    {
        $aResult = self::$_aStartNodeAttributes;
        if (Registry::getConfig()->getConfigParam('boolTRWSeoImages')) {
            $aResult = array_merge($aResult, self::$_aStartNodeAttributesForImages);
        }

        return $aResult;
    }

    /**
     * get the XML file path.
     *
     * @param bool $bRelative - the path relative to root?
     */
    public static function getSitemapFilePath(bool $bRelative = true): string
    {
        $sResult = '';
        if (!$bRelative) {
            $sResult .= Registry::getConfig()->getConfigParam('sShopDir');
        }
        $sResult .= self::$_sSiteMapFilePath;

        return $sResult;
    }

    /** get the Number of the sitemap-File */
    public static function getSitemapFileNumber(): int
    {
        $iFileNumber = (int) Registry::getConfig()->getConfigParam('numTRWSeoFileNumber');
        if ($iFileNumber === 0) {
            $iFileNumber = 1;
        }

        return $iFileNumber;
    }

    public static function isFileChangeNecessary(int $iEntry = 0): bool
    {
        $bResult = false;
        $iFileNumber = self::getSitemapFileNumber();
        if ($iEntry > ($iFileNumber * self::$_iSitemapMaxEntries)) {
            $bResult = true;
        }
        $sFile = self::getSitemapFilePath(false) . self::getSitemapFileName();
        if (file_exists($sFile) && (filesize($sFile) > self::$_iSitemapMaxFileSize)) {
            $bResult = true;
        }

        return $bResult;
    }
}
