<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Core;

use OxidEsales\Eshop\Core\DbMetaDataHandler;
use TheRealWorld\SchedulerModule\Core\SchedulerHelper;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

class SeoEvents
{
    /**
     * OXID-Core.
     */
    public static function onActivate()
    {
        $sCreateSql = "create table if not exists `trwseositemapexport` (
            `OXID` char(32) character set latin1 collate latin1_general_ci not null,
            `OXSHOPID` int(11) NOT NULL DEFAULT '1' COMMENT 'Shop id (oxshops)',
            `OXPARENTID` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'OxId of MainUrl',
            `OXOBJECTID` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Object id (table set by oxclass)',
            `OXCLASS` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Object table name',
            `OXTYPE` enum('page','pagealternative','image','video','audio') collate utf8_general_ci NOT NULL DEFAULT 'page' COMMENT 'Link Type',
            `OXLANG` int(2) unsigned NOT NULL COMMENT 'Language ID',
            `OXISMAINLOC` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'is this the Main-URL?',
            `OXLOC` varchar(255) NOT NULL DEFAULT '' COMMENT 'Location-Url',
            `OXLASTMOD` date NULL COMMENT 'Date, with last modification',
            `OXCHANGEFREQ` enum('always','hourly','daily','weekly','monthly','yearly','never') collate utf8_general_ci NOT NULL DEFAULT 'daily' COMMENT 'Change Frequence',
            `OXPRIORITY` decimal(2,1) unsigned NOT NULL DEFAULT '0.5' COMMENT 'Priority',
            `OXUPDATEPROGRESS` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Is this article in update-Progress',
            `OXTIMESTAMP` timestamp not null default current_timestamp on update current_timestamp,
            PRIMARY KEY (`OXID`),
            KEY `OXPARENTID` (`OXPARENTID`),
            KEY `OXTYPE` (`OXTYPE`),
            KEY `OXOBJECTID` (`OXOBJECTID`,`OXCLASS`)
            ) engine=InnoDB default CHARSET=utf8 comment 'the-real-world: Scheduler-Export for Sitemap'";
        ToolsDB::execute($sCreateSql);

        $sCreateSql = "create table if not exists `trwseoupdatemeta` (
            `OXID` char(32) character set latin1 collate latin1_general_ci not null,
            `OXOBJECTID` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Object id (table set by oxclass)',
            `OXCLASS` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Object table name',
            `OXNAMESPACECLASSNAME` varchar(255) character set latin1 collate latin1_general_ci not null default '' comment 'NamespaceClassName for Object',
            `OXUPDATEPROGRESS` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Is this article in update-Progress',
            `OXTIMESTAMP` timestamp not null default current_timestamp on update current_timestamp,
            PRIMARY KEY (`OXID`),
            KEY `OXOBJECTID` (`OXOBJECTID`,`OXCLASS`)
            ) engine=InnoDB default CHARSET=utf8 comment 'the-real-world: Scheduler-Update for MetaDeta'";
        ToolsDB::execute($sCreateSql);

        // additional Module-Update v1.4
        if (!ToolsDB::tableColumnExists('trwseositemapexport', 'OXSHOPID')) {
            $sAddSql = "ALTER TABLE `trwseositemapexport` ADD `OXSHOPID` int(11) NOT NULL DEFAULT '1' COMMENT 'Shop id (oxshops)'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxarticles', 'OXISSITEMAP')) {
            $sAddSql = "ALTER TABLE `oxarticles` ADD `OXISSITEMAP` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Should article be shown in sitemap'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxcategories', 'OXISSITEMAP')) {
            $sAddSql = "ALTER TABLE `oxcategories` ADD `OXISSITEMAP` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Should category be shown in sitemap'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxcontents', 'OXISSITEMAP')) {
            $sAddSql = "ALTER TABLE `oxcontents` ADD `OXISSITEMAP` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Should content be shown in sitemap'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxvendor', 'OXISSITEMAP')) {
            $sAddSql = "ALTER TABLE `oxvendor` ADD `OXISSITEMAP` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Should vendor be shown in sitemap'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxmanufacturers', 'OXISSITEMAP')) {
            $sAddSql = "ALTER TABLE `oxmanufacturers` ADD `OXISSITEMAP` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Should manufacturers be shown in sitemap'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxarticles', 'OXSEOTITLE')) {
            $sAddSql = "ALTER TABLE `oxarticles` ADD `OXSEOTITLE` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for articles'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxarticles', 'OXSEOTITLE_1')) {
            $sAddSql = "ALTER TABLE `oxarticles` ADD `OXSEOTITLE_1` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for articles'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxarticles', 'OXSEOTITLE_2')) {
            $sAddSql = "ALTER TABLE `oxarticles` ADD `OXSEOTITLE_2` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for articles'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxarticles', 'OXSEOTITLE_3')) {
            $sAddSql = "ALTER TABLE `oxarticles` ADD `OXSEOTITLE_3` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for articles'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxcategories', 'OXSEOTITLE')) {
            $sAddSql = "ALTER TABLE `oxcategories` ADD `OXSEOTITLE` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for categories'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxcategories', 'OXSEOTITLE_1')) {
            $sAddSql = "ALTER TABLE `oxcategories` ADD `OXSEOTITLE_1` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for categories'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxcategories', 'OXSEOTITLE_2')) {
            $sAddSql = "ALTER TABLE `oxcategories` ADD `OXSEOTITLE_2` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for categories'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxcategories', 'OXSEOTITLE_3')) {
            $sAddSql = "ALTER TABLE `oxcategories` ADD `OXSEOTITLE_3` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for categories'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxcontents', 'OXSEOTITLE')) {
            $sAddSql = "ALTER TABLE `oxcontents` ADD `OXSEOTITLE` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for contents'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxcontents', 'OXSEOTITLE_1')) {
            $sAddSql = "ALTER TABLE `oxcontents` ADD `OXSEOTITLE_1` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for contents'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxcontents', 'OXSEOTITLE_2')) {
            $sAddSql = "ALTER TABLE `oxcontents` ADD `OXSEOTITLE_2` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for contents'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxcontents', 'OXSEOTITLE_3')) {
            $sAddSql = "ALTER TABLE `oxcontents` ADD `OXSEOTITLE_3` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for contents'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxvendor', 'OXSEOTITLE')) {
            $sAddSql = "ALTER TABLE `oxvendor` ADD `OXSEOTITLE` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for vendors'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxvendor', 'OXSEOTITLE_1')) {
            $sAddSql = "ALTER TABLE `oxvendor` ADD `OXSEOTITLE_1` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for vendors'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxvendor', 'OXSEOTITLE_2')) {
            $sAddSql = "ALTER TABLE `oxvendor` ADD `OXSEOTITLE_2` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for vendors'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxvendor', 'OXSEOTITLE_3')) {
            $sAddSql = "ALTER TABLE `oxvendor` ADD `OXSEOTITLE_3` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for vendors'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxmanufacturers', 'OXSEOTITLE')) {
            $sAddSql = "ALTER TABLE `oxmanufacturers` ADD `OXSEOTITLE` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for manufactures'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxmanufacturers', 'OXSEOTITLE_1')) {
            $sAddSql = "ALTER TABLE `oxmanufacturers` ADD `OXSEOTITLE_1` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for manufactures'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxmanufacturers', 'OXSEOTITLE_2')) {
            $sAddSql = "ALTER TABLE `oxmanufacturers` ADD `OXSEOTITLE_2` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for manufactures'";
            ToolsDB::execute($sAddSql);
        }

        if (!ToolsDB::tableColumnExists('oxmanufacturers', 'OXSEOTITLE_3')) {
            $sAddSql = "ALTER TABLE `oxmanufacturers` ADD `OXSEOTITLE_3` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Seotitle for manufactures'";
            ToolsDB::execute($sAddSql);
        }

        $oMetaData = oxNew(DbMetaDataHandler::class);
        $oMetaData->updateViews();

        return true;
    }

    /**
     * OXID-Core.
     */
    public static function onDeactivate()
    {
        // for security set return true
        return true;
        $sDeleteSql = 'drop table if exists `trwseositemapexport`';
        ToolsDB::execute($sDeleteSql);

        if (ToolsDB::tableColumnExists('oxarticles', 'OXISSITEMAP')) {
            $sDeleteSql = 'ALTER TABLE `oxarticles` DROP `OXISSITEMAP`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxarticles', 'OXSEOTITLE')) {
            $sDeleteSql = 'ALTER TABLE `oxarticles` DROP `OXSEOTITLE`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxarticles', 'OXSEOTITLE_1')) {
            $sDeleteSql = 'ALTER TABLE `oxarticles` DROP `OXSEOTITLE_1`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxarticles', 'OXSEOTITLE_2')) {
            $sDeleteSql = 'ALTER TABLE `oxarticles` DROP `OXSEOTITLE_2`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxarticles', 'OXSEOTITLE_3')) {
            $sDeleteSql = 'ALTER TABLE `oxarticles` DROP `OXSEOTITLE_3`';
            ToolsDB::execute($sDeleteSql);
        }

        if (ToolsDB::tableColumnExists('oxcategories', 'OXISSITEMAP')) {
            $sDeleteSql = 'ALTER TABLE `oxcategories` DROP `OXISSITEMAP`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxcategories', 'OXSEOTITLE')) {
            $sDeleteSql = 'ALTER TABLE `oxcategories` DROP `OXSEOTITLE`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxcategories', 'OXSEOTITLE_1')) {
            $sDeleteSql = 'ALTER TABLE `oxcategories` DROP `OXSEOTITLE_1`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxcategories', 'OXSEOTITLE_2')) {
            $sDeleteSql = 'ALTER TABLE `oxcategories` DROP `OXSEOTITLE_2`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxcategories', 'OXSEOTITLE_3')) {
            $sDeleteSql = 'ALTER TABLE `oxcategories` DROP `OXSEOTITLE_3`';
            ToolsDB::execute($sDeleteSql);
        }

        if (ToolsDB::tableColumnExists('oxcontents', 'OXISSITEMAP')) {
            $sDeleteSql = 'ALTER TABLE `oxcontents` DROP `OXISSITEMAP`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxcontents', 'OXSEOTITLE')) {
            $sDeleteSql = 'ALTER TABLE `oxcontents` DROP `OXSEOTITLE`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxcontents', 'OXSEOTITLE_1')) {
            $sDeleteSql = 'ALTER TABLE `oxcontents` DROP `OXSEOTITLE_1`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxcontents', 'OXSEOTITLE_2')) {
            $sDeleteSql = 'ALTER TABLE `oxcontents` DROP `OXSEOTITLE_2`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxcontents', 'OXSEOTITLE_3')) {
            $sDeleteSql = 'ALTER TABLE `oxcontents` DROP `OXSEOTITLE_3`';
            ToolsDB::execute($sDeleteSql);
        }

        if (ToolsDB::tableColumnExists('oxvendor', 'OXISSITEMAP')) {
            $sDeleteSql = 'ALTER TABLE `oxvendor` DROP `OXISSITEMAP`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxvendor', 'OXSEOTITLE')) {
            $sDeleteSql = 'ALTER TABLE `oxvendor` DROP `OXSEOTITLE`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxvendor', 'OXSEOTITLE_1')) {
            $sDeleteSql = 'ALTER TABLE `oxvendor` DROP `OXSEOTITLE_1`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxvendor', 'OXSEOTITLE_2')) {
            $sDeleteSql = 'ALTER TABLE `oxvendor` DROP `OXSEOTITLE_2`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxvendor', 'OXSEOTITLE_3')) {
            $sDeleteSql = 'ALTER TABLE `oxvendor` DROP `OXSEOTITLE_3`';
            ToolsDB::execute($sDeleteSql);
        }

        if (ToolsDB::tableColumnExists('oxmanufacturers', 'OXISSITEMAP')) {
            $sDeleteSql = 'ALTER TABLE `oxmanufacturers` DROP `OXISSITEMAP`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxmanufacturers', 'OXSEOTITLE')) {
            $sDeleteSql = 'ALTER TABLE `oxmanufacturers` DROP `OXSEOTITLE`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxmanufacturers', 'OXSEOTITLE_1')) {
            $sDeleteSql = 'ALTER TABLE `oxmanufacturers` DROP `OXSEOTITLE_1`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxmanufacturers', 'OXSEOTITLE_2')) {
            $sDeleteSql = 'ALTER TABLE `oxmanufacturers` DROP `OXSEOTITLE_2`';
            ToolsDB::execute($sDeleteSql);
        }
        if (ToolsDB::tableColumnExists('oxmanufacturers', 'OXSEOTITLE_3')) {
            $sDeleteSql = 'ALTER TABLE `oxmanufacturers` DROP `OXSEOTITLE_3`';
            ToolsDB::execute($sDeleteSql);
        }

        $oMetaData = oxNew(DbMetaDataHandler::class);
        $oMetaData->updateViews();

        // delete Task
        SchedulerHelper::deleteTask('TaskSeoSitemapExport');

        return true;
    }
}
