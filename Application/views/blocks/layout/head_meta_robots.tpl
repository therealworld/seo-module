[{if $oView|method_exists:'isNoIndex'}]
    [{if $oView->isNoIndex()}]
        <meta name="robots" content="NOINDEX, FOLLOW" />
    [{/if}]
[{else}]
    [{$smarty.block.parent}]
[{/if}]