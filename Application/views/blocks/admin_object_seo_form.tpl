[{$smarty.block.parent}]
[{if $isEnabledForSeo}]
<tr>
    <td class="edittext">
        [{oxmultilang ident="GENERAL_ARTICLE_OXSEOTITLE"}]
    </td>
    <td class="edittext">
        <input type="text" class="editinput" style="width: 100%;" name="aSeoData[oxseotitle]" value="[{$oView->getSeoTitle()}]" [{$readonly}]>
        [{oxinputhelp ident="HELP_GENERAL_ARTICLE_OXSEOTITLE"}]
    </td>
</tr>
<tr>
    <td class="edittext">
        [{oxmultilang ident="GENERAL_ARTICLE_OXISSITEMAP"}]
    </td>
    <td class="edittext">
        <input class="edittext" type="checkbox" name="aSeoData[oxissitemap]" value="1" [{if $oView->isSitemap()}]checked[{/if}] [{$readonly}]>
        [{oxinputhelp ident="HELP_GENERAL_ARTICLE_OXISSITEMAP"}]
    </td>
</tr>
[{/if}]
