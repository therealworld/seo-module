[{if $oView|method_exists:'getAdditionalSeoLanguages' && $module_var=='arrTRWSeoLanguages'}]
    [{assign var="aLangs" value=$oView->getAdditionalSeoLanguages()}]
    <input id="input_confarrs_[{$module_var}]" type="hidden" name="confarrs[[{$module_var}]]" value="" />
    <select multiple style="width: 210px;" onChange="transferSelected[{$module_var}](this)">
        [{foreach from=$aLangs key=sKey item=oLang}]
            <option value="[{$oLang->abbr}]" [{if $oLang->selected}]selected[{/if}]>[{$oLang->name}]</option>
        [{/foreach}]
    </select>
    [{capture assign="sTransferJS"}]
        [{strip}]
            function transferSelected[{$module_var}](sel) {
                var opts = [],
                    opt,
                    len = sel.options.length;
                for (var i = 0; i < len; i++) {
                    opt = sel.options[i];
                    if (opt.selected) {
                        opts.push(opt.value);
                    }
                }
                var elem = document.getElementById('input_confarrs_[{$module_var}]');
                elem.value = opts.join(" \n ");
                console.log(elem.value);
            }
        [{/strip}]
    [{/capture}]
    [{oxscript add=$sTransferJS priority=10}]
[{else}]
    [{$smarty.block.parent}]
[{/if}]
