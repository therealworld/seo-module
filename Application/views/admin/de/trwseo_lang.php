<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

include_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' .
    DIRECTORY_SEPARATOR . 'translations' . DIRECTORY_SEPARATOR .
    'de' .
    DIRECTORY_SEPARATOR . basename(__FILE__);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array_merge(
    $aLang,
    [
        'GENERAL_ARTICLE_OXSEOTITLE'       => 'SEO Meta Titel',
        'HELP_GENERAL_ARTICLE_OXSEOTITLE'  => 'Wenn Feld gefüllt ist, dann wird der von OXID generierte Titel durch diesen Titel überschrieben.',
        'GENERAL_ARTICLE_OXISSITEMAP'      => 'sitemap.xml?',
        'HELP_GENERAL_ARTICLE_OXISSITEMAP' => 'Soll diese URL in die sitemap.xml eingetragen werden?',
    ]
);
