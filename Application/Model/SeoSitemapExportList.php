<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Model;

use OxidEsales\Eshop\Core\Model\ListModel;
use OxidEsales\Eshop\Core\Registry;

/**
 * Seo SitemapList class.
 */
class SeoSitemapExportList extends ListModel
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sObjectsInListName = 'TheRealWorld\SeoModule\Application\Model\SeoSitemapExport';

    /**
     * return fields to select while loading category tree.
     *
     * @param int $iStep  - Start position for SQL
     * @param int $iLimit - Limit position for SQL
     */
    public function loadSitemapExportList(int $iStep = 0, int $iLimit = 0): void
    {
        $oBaseObject = oxNew(SeoSitemapExport::class);
        $sTable = $oBaseObject->getViewName();

        $sSelect = "select {$sTable}.`oxid`, {$sTable}.`oxloc`, {$sTable}.`oxlastmod`,
            {$sTable}.`oxchangefreq`, {$sTable}.`oxpriority`
            from {$sTable}
            where {$sTable}.oxparentid = '' and {$sTable}.oxshopid = " . Registry::getConfig()->getShopId() . '
            limit ' . $iStep . ', ' . $iLimit;

        $this->selectString($sSelect);
    }
}
