<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Model;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\ObjectException;
use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Model\BaseModel;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * Manages object assignment to SeoData.
 */
class Object2SeoData extends BaseModel
{
    use DataGetter;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_iLanguage = null;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sExistKey = 'oxobjectid';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_aFieldNames = ['oxobjectid' => 0];

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sClassName = 'TheRealWorld\SeoModule\Application\Model\Object2SeoData';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function __construct()
    {
        parent::__construct();
        $this->init('oxobject2seodata');
    }

    /**
     * load an object by objectId and language id.
     */
    public function loadByObjectId(?string $sOxObjectId = null, ?int $iLangId = null): bool
    {
        $iLangId = $iLangId ?? (int) Registry::getLang()->getBaseLanguage();

        $query = $this->buildSelectString([
            $this->getViewName() . '.oxobjectid' => $sOxObjectId,
            $this->getViewName() . '.oxlang'     => $iLangId,
            $this->getViewName() . '.oxshopid'   => Registry::getConfig()->getShopId(),
        ]);
        $this->_isLoaded = $this->assignRecord($query);
        $this->setId($sOxObjectId);
        $this->setLanguage($iLangId);

        return $this->_isLoaded;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function setId($sOxObjectId = null)
    {
        if ($sOxObjectId) {
            $this->_sOXID = $sOxObjectId;
        } else {
            $this->_sOXID = Registry::getUtilsObject()->generateUID();
        }

        $idFieldName = $this->getCoreTableName() . '__' . $this->_sExistKey;
        $this->{$idFieldName} = new Field($this->_sOXID, Field::T_RAW);

        return $this->_sOXID;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function assign($dbRecord)
    {
        if (!is_array($dbRecord)) {
            return;
        }

        foreach ($dbRecord as $name => $value) {
            $this->_setFieldData($name, $value);
        }

        $oxidField = $this->_getFieldLongName($this->_sExistKey);
        if ($this->{$oxidField} instanceof Field) {
            $this->_sOXID = $this->getTRWStringData($oxidField);
        }
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected function _update() // phpcs:ignore PSR2.Methods.MethodDeclaration.Underscore
    {
        // do not allow derived item update
        if (!$this->allowDerivedUpdate()) {
            return false;
        }

        if (!$this->getId()) {
            $exception = oxNew(ObjectException::class);
            $exception->setMessage('EXCEPTION_OBJECT_OXIDNOTSET');
            $exception->setObject($this);

            throw $exception;
        }
        $coreTableName = $this->getCoreTableName();

        $idKey = Registry::getUtils()->getArrFldName($coreTableName . '.' . $this->_sExistKey);
        $this->{$idKey} = new Field($this->getId(), Field::T_RAW);
        $oDb = DatabaseProvider::getDb();

        $updateQuery = "update {$coreTableName} set " . $this->_getUpdateFields() .
            " where {$coreTableName}." . $this->_sExistKey . " = " . $oDb->quote($this->getId()) .
            " and {$coreTableName}.oxlang = " . $this->getTRWIntData('oxlang') . " ";

        if (ToolsConfig::isMultiShop()) {
            $updateQuery .= " and {$coreTableName}.oxshopid = " . Registry::getConfig()->getShopId();
        }

        $this->beforeUpdate();

        $this->executeDatabaseQuery(
            $updateQuery
        );

        return true;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function setLanguage($lang = null)
    {
        $this->_iLanguage = (int) $lang;
        $this->assign(['oxlang' => $this->_iLanguage]);
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function getLanguage()
    {
        if (is_null($this->_iLanguage)) {
            $this->_iLanguage = $this->getTRWIntData('oxlang');
        }
        return $this->_iLanguage;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function exists($oxid = null)
    {
        if (!$oxid) {
            $oxid = $this->getId();
        }
        if (!$oxid) {
            return false;
        }
        $iLang = $this->getLanguage();

        $viewName = $this->getCoreTableName();
        $database = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $query = "select {$this->_sExistKey} from {$viewName} where {$this->_sExistKey} = :oxid and oxlang = :oxlang";

        return (bool) $database->getOne($query, [
            ':oxid' => $oxid,
            ':oxlang' => $iLang
        ]);
    }
}
