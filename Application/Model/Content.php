<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Model;

use Exception;
use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * Slider Content class.
 *
 * @mixin \OxidEsales\Eshop\Application\Model\Content
 */
class Content extends Content_parent
{
    use DataGetter;

    // Ability to control the timing of saving SEO data
    protected bool $_bSaveSeoData = true;

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function save()
    {
        if ($this->getSaveSeoData()) {
            $oLang = Registry::getLang();
            $aLanguages = $oLang->getAllShopLanguageIds();
            $iLang = $this->getLanguage();
            $sLang = $aLanguages[$iLang];

            $oConfig = Registry::getConfig();
            $aMetaContentTitle = $oConfig->getConfigParam('aarrTRWSeoMetaContentTitle');
            $aMetaContentDesc = $oConfig->getConfigParam('aarrTRWSeoMetaContentDesc');
            $aMetaContentKeywords = $oConfig->getConfigParam('aarrTRWSeoMetaContentKeywords');

            $oObject2SeoData = oxNew(Object2SeoData::class);

            if (!$this->getTRWStringData('oxseotitle')) {
                $sDefaultMetaContentTitle = $aMetaContentTitle[$sLang] ?: '';
                $sMetaContentTitle = str_replace(
                    '%title',
                    $this->getTRWStringData('oxtitle'),
                    $sDefaultMetaContentTitle
                );

                // create a simple method replaceAdditonalSeoContent in another module
                if (method_exists($this, 'replaceAdditonalSeoContent')) {
                    $sMetaContentTitle = $this->replaceAdditonalSeoContent($sMetaContentTitle);
                }

                $this->oxcontents__oxseotitle = new Field($sMetaContentTitle, Field::T_RAW);
            }

            $bChangeDesc = true;
            $bChangeKeywords = true;
            $oObject2SeoData->loadByObjectId($this->getId(), $iLang);

            if ($oObject2SeoData->isLoaded()) {
                $bChangeDesc = !$oObject2SeoData->getTRWStringData('oxdescription');
                $bChangeKeywords = !$oObject2SeoData->getTRWStringData('oxkeywords');
            }

            if ($bChangeDesc) {
                $sMetaContentDesc = $this->replaceSeo($aMetaContentDesc[$sLang] ?: '');

                // create a simple method replaceAdditonalSeoContent in another module
                if (method_exists($this, 'replaceAdditonalSeoContent')) {
                    $sMetaContentDesc = $this->replaceAdditonalSeoContent($sMetaContentDesc);
                }

                $oObject2SeoData->oxobject2seodata__oxdescription = new Field($sMetaContentDesc, Field::T_RAW);
            }

            if ($bChangeKeywords) {
                $sMetaContentKeywords = $this->replaceSeo($aMetaContentKeywords[$sLang] ?: '');

                // create a simple method replaceAdditonalSeoContent in another module
                if (method_exists($this, 'replaceAdditonalSeoContent')) {
                    $sMetaContentKeywords = $this->replaceAdditonalSeoContent($sMetaContentKeywords);
                }

                $oObject2SeoData->oxobject2seodata__oxkeywords = new Field($sMetaContentKeywords, Field::T_RAW);
            }

            $oObject2SeoData->save();
        }

        return parent::save();
    }

    /**
     * Ability to control the timing of saving SEO data
     *
     * @param bool $bValue - switch on/off
     */
    public function setSaveSeoData(bool $bValue = true): void
    {
        $this->_bSaveSeoData = $bValue;
    }

    /**
     * Ability to control the timing of saving SEO data
     */
    public function getSaveSeoData(): bool
    {
        return $this->_bSaveSeoData;
    }

    protected function replaceSeo(string $sDefault = ''): string
    {
        return str_replace(
            [
                '%title',
                '%desc'
            ],
            [
                $this->getTRWStringData('oxtitle'),
                Str::getStr()->strip_tags($this->getTRWRawStringData('oxcontent')),
            ],
            $sDefault
        );
    }
}
