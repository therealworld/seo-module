<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Model;

use Exception;
use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * Slider Article class.
 *
 * @mixin \OxidEsales\Eshop\Application\Model\Article
 */
class Article extends Article_parent
{
    use DataGetter;

    // Ability to control the timing of saving SEO data
    protected bool $_bSaveSeoData = true;

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function save()
    {
        if ($this->getSaveSeoData()) {
            $oLang = Registry::getLang();
            $aLanguages = $oLang->getAllShopLanguageIds();
            $iLang = $this->getLanguage();
            $sLang = $aLanguages[$iLang];

            $oConfig = Registry::getConfig();
            $aMetaArticleTitle = $oConfig->getConfigParam('aarrTRWSeoMetaArticleTitle');
            $aMetaArticleDesc = $oConfig->getConfigParam('aarrTRWSeoMetaArticleDesc');
            $aMetaArticleKeywords = $oConfig->getConfigParam('aarrTRWSeoMetaArticleKeywords');

            $oObject2SeoData = oxNew(Object2SeoData::class);

            if (!$this->getTRWStringData('oxseotitle')) {
                $sDefaultMetaArticleTitle = $aMetaArticleTitle[$sLang] ?: '';
                $sMetaArticleTitle = str_replace(
                    '%title',
                    $this->getTRWStringData('oxtitle'),
                    $sDefaultMetaArticleTitle
                );

                if (method_exists($this, 'replaceAdditonalSeoContent')) {
                    $sMetaArticleTitle = $this->replaceAdditonalSeoContent($sMetaArticleTitle);
                }

                $this->oxarticles__oxseotitle = new Field($sMetaArticleTitle, Field::T_RAW);
            }

            $bChangeDesc = true;
            $bChangeKeywords = true;
            $oObject2SeoData->loadByObjectId($this->getId(), $iLang);

            if ($oObject2SeoData->isLoaded()) {
                $bChangeDesc = !$oObject2SeoData->getTRWStringData('oxdescription');
                $bChangeKeywords = !$oObject2SeoData->getTRWStringData('oxkeywords');
            }

            if ($bChangeDesc) {
                $sMetaArticleDesc = $this->replaceSeo($aMetaArticleDesc[$sLang] ?: '');

                if (method_exists($this, 'replaceAdditonalSeoContent')) {
                    $sMetaArticleDesc = $this->replaceAdditonalSeoContent($sMetaArticleDesc);
                }

                $oObject2SeoData->oxobject2seodata__oxdescription = new Field($sMetaArticleDesc, Field::T_RAW);
            }
            if ($bChangeKeywords) {
                $sMetaArticleKeywords = $this->replaceSeo($aMetaArticleKeywords[$sLang] ?: '');

                if (method_exists($this, 'replaceAdditonalSeoContent')) {
                    $sMetaArticleKeywords = $this->replaceAdditonalSeoContent($sMetaArticleKeywords);
                }

                $oObject2SeoData->oxobject2seodata__oxkeywords = new Field($sMetaArticleKeywords, Field::T_RAW);
            }
            $oObject2SeoData->save();
        }

        return parent::save();
    }

    /**
     * Ability to control the timing of saving SEO data
     *
     * @param bool $bValue - switch on/off
     */
    public function setSaveSeoData(bool $bValue = true): void
    {
        $this->_bSaveSeoData = $bValue;
    }

    /**
     * Ability to control the timing of saving SEO data
     */
    public function getSaveSeoData(): bool
    {
        return $this->_bSaveSeoData;
    }

    protected function replaceSeo(string $sDefault = ''): string
    {
        $sLongDesc = $this->getLongDescription() ?? '';

        return str_replace(
            [
                '%title',
                '%shortdesc',
                '%longdesc',
                '%keywords'
            ],
            [
                $this->getTRWStringData('oxtitle'),
                $this->getTRWStringData('oxshortdesc'),
                Str::getStr()->strip_tags($sLongDesc),
                $this->getTRWStringData('oxsearchkeys'),
            ],
            $sDefault
        );
    }
}
