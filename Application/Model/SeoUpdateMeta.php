<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Model;

use OxidEsales\Eshop\Core\Model\BaseModel;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * Seo Sitemap class.
 */
class SeoUpdateMeta extends BaseModel
{
    use DataGetter;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_blUseLazyLoading = true;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sClassName = '\TheRealWorld\SeoModule\Application\Model\SeoUpdateMeta';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function __construct()
    {
        parent::__construct();
        $this->init('trwseoupdatemeta');
    }
}
