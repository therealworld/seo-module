<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Model;

use Exception;
use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * Slider Category class.
 *
 * @mixin \OxidEsales\Eshop\Application\Model\Category
 */
class Category extends Category_parent
{
    use DataGetter;

    // Ability to control the timing of saving SEO data
    protected bool $_bSaveSeoData = true;

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function save()
    {
        if ($this->getSaveSeoData()) {
            $oLang = Registry::getLang();
            $aLanguages = $oLang->getAllShopLanguageIds();
            $iLang = $this->getLanguage();
            $sLang = $aLanguages[$iLang];

            $oConfig = Registry::getConfig();
            $aMetaCategoryTitle = $oConfig->getConfigParam('aarrTRWSeoMetaCategoryTitle');
            $aMetaCategoryDesc = $oConfig->getConfigParam('aarrTRWSeoMetaCategoryDesc');
            $aMetaCategoryKeywords = $oConfig->getConfigParam('aarrTRWSeoMetaCategoryKeywords');

            $oObject2SeoData = oxNew(Object2SeoData::class);

            if (!$this->getTRWStringData('oxseotitle')) {
                $sDefaultMetaCategoryTitle = $aMetaCategoryTitle[$sLang] ?: '';
                $sMetaCategoryTitle = str_replace(
                    '%title',
                    $this->getTRWStringData('oxtitle'),
                    $sDefaultMetaCategoryTitle
                );

                // create a simple method replaceAdditonalSeoContent in another module
                if (method_exists($this, 'replaceAdditonalSeoContent')) {
                    $sMetaCategoryTitle = $this->replaceAdditonalSeoContent($sMetaCategoryTitle);
                }

                $this->oxcategories__oxseotitle = new Field($sMetaCategoryTitle, Field::T_RAW);
            }

            $bChangeDesc = true;
            $bChangeKeywords = true;
            $oObject2SeoData->loadByObjectId($this->getId(), $iLang);

            if ($oObject2SeoData->isLoaded()) {
                $bChangeDesc = !$oObject2SeoData->getTRWStringData('oxdescription');
                $bChangeKeywords = !$oObject2SeoData->getTRWStringData('oxkeywords');
            }

            if ($bChangeDesc) {
                $sMetaCategoryDesc = $this->replaceSeo($aMetaCategoryDesc[$sLang] ?: '');

                // create a simple method replaceAdditonalSeoContent in another module
                if (method_exists($this, 'replaceAdditonalSeoContent')) {
                    $sMetaCategoryDesc = $this->replaceAdditonalSeoContent($sMetaCategoryDesc);
                }

                $oObject2SeoData->oxobject2seodata__oxdescription = new Field($sMetaCategoryDesc, Field::T_RAW);
            }

            if ($bChangeKeywords) {
                $sMetaCategoryKeywords = $this->replaceSeo($aMetaCategoryKeywords[$sLang] ?: '');

                // create a simple method replaceAdditonalSeoContent in another module
                if (method_exists($this, 'replaceAdditonalSeoContent')) {
                    $sMetaCategoryKeywords = $this->replaceAdditonalSeoContent($sMetaCategoryKeywords);
                }

                $oObject2SeoData->oxobject2seodata__oxkeywords = new Field($sMetaCategoryKeywords, Field::T_RAW);
            }

            $oObject2SeoData->save();
        }

        return parent::save();
    }

    /**
     * Ability to control the timing of saving SEO data
     *
     * @param bool $bValue - switch on/off
     */
    public function setSaveSeoData(bool $bValue = true): void
    {
        $this->_bSaveSeoData = $bValue;
    }

    /**
     * Ability to control the timing of saving SEO data
     */
    public function getSaveSeoData(): bool
    {
        return $this->_bSaveSeoData;
    }

    protected function replaceSeo(string $sDefault = ''): string
    {
        return str_replace(
            [
                '%title',
                '%shortdesc',
                '%longdesc'
            ],
            [
                $this->getTRWStringData('oxtitle'),
                $this->getTRWStringData('oxdesc'),
                Str::getStr()->strip_tags($this->getTRWRawStringData('oxlongdesc')),
            ],
            $sDefault
        );
    }
}
