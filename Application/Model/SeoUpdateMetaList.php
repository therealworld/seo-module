<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Model;

use OxidEsales\Eshop\Core\Model\ListModel;

/**
 * Seo SitemapList class.
 */
class SeoUpdateMetaList extends ListModel
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sObjectsInListName = 'TheRealWorld\SeoModule\Application\Model\SeoUpdateMeta';

    /**
     * return fields to select while loading category tree.
     *
     * @param int $iStep  - Start position for SQL
     * @param int $iLimit - Limit position for SQL
     */
    public function loadUpdateMetaList(int $iStep = 0, int $iLimit = 0): void
    {
        $oBaseObject = oxNew(SeoUpdateMeta::class);
        $sTable = $oBaseObject->getViewName();

        $sSelect = "select {$sTable}.*
            from {$sTable}
            limit " . $iStep . ", " . $iLimit;

        $this->selectString($sSelect);
    }
}
