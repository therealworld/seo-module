<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Model;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Model\BaseModel;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * Seo Sitemap class.
 */
class SeoSitemapExport extends BaseModel
{
    use DataGetter;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_blUseLazyLoading = true;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sClassName = '\TheRealWorld\SeoModule\Application\Model\SeoSitemapExport';

    /** should be added language Urls */
    protected bool $_bAddLanguageUrls = false;

    /** should be added image Urls */
    protected bool $_bAddImageUrls = false;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function __construct()
    {
        parent::__construct();
        $this->init('trwseositemapexport');
        $aSelectedLanguages = ToolsConfig::getOptionsArray('arrTRWSeoLanguages');
        if (count($aSelectedLanguages)) {
            $this->_bAddLanguageUrls = true;
        }
        if (Registry::getConfig()->getConfigParam('boolTRWSeoImages')) {
            $this->_bAddImageUrls = true;
        }
    }

    /** get Addtional Data for a sitemapexport-Line */
    public function getAdditionalData(): array
    {
        $aResult = [];
        if ($this->_bAddLanguageUrls && ($aLang = $this->_getAlternativeLanguageData())) {
            $aResult = array_merge($aResult, $aLang);
        }
        if ($this->_bAddImageUrls && ($aImage = $this->_getImageData())) {
            $aResult = array_merge($aResult, $aImage);
        }

        return $aResult;
    }

    /** get the alternative language Url Data.
     * @throws DatabaseConnectionException
     * @throws DatabaseErrorException
     */
    protected function _getAlternativeLanguageData(): array
    {
        $aResult = [];
        if ($sOxid = $this->getId()) {
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
            $sTable = $this->getViewName();
            $sSelect = "select `oxloc`, `oxlang`
                from {$sTable}
                where `oxparentid` = " . $oDb->quote($sOxid) . "
                and `oxtype` = 'pagealternative'";

            $oResults = $oDb->select($sSelect);
            if ($oResults && $oResults->count() > 0) {
                while (!$oResults->EOF) {
                    $aRow = $oResults->getFields();
                    $aResult['xhtml:link'][] = [
                        '@rel'      => 'alternate',
                        '@hreflang' => Registry::getLang()->getLanguageAbbr($aRow['oxlang']),
                        '@href'     => $aRow['oxloc'],
                    ];
                    $oResults->fetchRow();
                }
            }
        }

        return $aResult;
    }

    /** get image Url Data.
     * @throws DatabaseErrorException
     * @throws DatabaseConnectionException
     */
    protected function _getImageData(): array
    {
        $aResult = [];
        if ($sOxid = $this->getId()) {
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
            $sTable = $this->getViewName();
            $sSelect = "select `oxloc`, `oxlang`
                from {$sTable}
                where `oxparentid` = " . $oDb->quote($sOxid) . "
                and `oxtype` = 'image'";

            $oResults = $oDb->select($sSelect);
            if ($oResults && $oResults->count() > 0) {
                while (!$oResults->EOF) {
                    $aRow = $oResults->getFields();
                    $aResult['image:image'][] = [
                        'image:loc' => $aRow['oxloc'],
                    ];
                    $oResults->fetchRow();
                }
            }
        }

        return $aResult;
    }
}
