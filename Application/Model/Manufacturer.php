<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Model;

use Exception;
use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * Slider Manufacturer class.
 *
 * @mixin \OxidEsales\Eshop\Application\Model\Manufacturer
 */
class Manufacturer extends Manufacturer_parent
{
    use DataGetter;

    // Ability to control the timing of saving SEO data
    protected bool $_bSaveSeoData = true;

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function save()
    {
        if ($this->getSaveSeoData()) {
            $oLang = Registry::getLang();
            $aLanguages = $oLang->getAllShopLanguageIds();
            $iLang = $this->getLanguage();
            $sLang = $aLanguages[$iLang];

            $oConfig = Registry::getConfig();
            $aMetaManufacturerTitle = $oConfig->getConfigParam('aarrTRWSeoMetaManufacturerTitle');
            $aMetaManufacturerDesc = $oConfig->getConfigParam('aarrTRWSeoMetaManufacturerDesc');
            $aMetaManufacturerKeywords = $oConfig->getConfigParam('aarrTRWSeoMetaManufacturerKeywords');

            $oObject2SeoData = oxNew(Object2SeoData::class);

            if (!$this->getTRWStringData('oxseotitle')) {
                $sDefaultMetaManufacturerTitle = $aMetaManufacturerTitle[$sLang] ?: '';
                $sMetaManufacturerTitle = str_replace(
                    '%title',
                    $this->getTRWStringData('oxtitle'),
                    $sDefaultMetaManufacturerTitle
                );

                // create a simple method replaceAdditonalSeoContent in another module
                if (method_exists($this, 'replaceAdditonalSeoContent')) {
                    $sMetaManufacturerTitle = $this->replaceAdditonalSeoContent($sMetaManufacturerTitle);
                }

                $this->oxmanufacturers__oxseotitle = new Field($sMetaManufacturerTitle, Field::T_RAW);
            }

            $bChangeDesc = true;
            $bChangeKeywords = true;
            $oObject2SeoData->loadByObjectId($this->getId(), $iLang);

            if ($oObject2SeoData->isLoaded()) {
                $bChangeDesc = !$oObject2SeoData->getTRWStringData('oxdescription');
                $bChangeKeywords = !$oObject2SeoData->getTRWStringData('oxkeywords');
            }

            if ($bChangeDesc) {
                $sMetaManufacturerDesc = $this->replaceSeo($aMetaManufacturerDesc[$sLang] ?: '');

                // create a simple method replaceAdditonalSeoContent in another module
                if (method_exists($this, 'replaceAdditonalSeoContent')) {
                    $sMetaManufacturerDesc = $this->replaceAdditonalSeoContent($sMetaManufacturerDesc);
                }
                $oObject2SeoData->oxobject2seodata__oxdescription = new Field($sMetaManufacturerDesc, Field::T_RAW);
            }

            if ($bChangeKeywords) {
                $sMetaManufacturerKeywords = $this->replaceSeo($aMetaManufacturerKeywords[$sLang] ?: '');

                // create a simple method replaceAdditonalSeoContent in another module
                if (method_exists($this, 'replaceAdditonalSeoContent')) {
                    $sMetaManufacturerKeywords = $this->replaceAdditonalSeoContent($sMetaManufacturerKeywords);
                }
                $oObject2SeoData->oxobject2seodata__oxkeywords = new Field($sMetaManufacturerKeywords, Field::T_RAW);
            }

            $oObject2SeoData->save();
        }

        return parent::save();
    }

    /**
     * Ability to control the timing of saving SEO data
     *
     * @param bool $bValue - switch on/off
     */
    public function setSaveSeoData(bool $bValue = true): void
    {
        $this->_bSaveSeoData = $bValue;
    }

    /**
     * Ability to control the timing of saving SEO data
     */
    public function getSaveSeoData(): bool
    {
        return $this->_bSaveSeoData;
    }

    protected function replaceSeo(string $sDefault = ''): string
    {
        return str_replace(
            [
                '%title',
                '%desc'
            ],
            [
                $this->getTRWStringData('oxtitle'),
                Str::getStr()->strip_tags($this->getTRWRawStringData('oxshortdesc')),
            ],
            $sDefault
        );
    }
}
