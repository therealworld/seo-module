<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Model;

use Exception;
use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * Slider Vendor class.
 *
 * @mixin \OxidEsales\Eshop\Application\Model\Vendor
 */
class Vendor extends Vendor_parent
{
    use DataGetter;

    // Ability to control the timing of saving SEO data
    protected bool $_bSaveSeoData = true;

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function save()
    {
        if ($this->getSaveSeoData()) {
            $oLang = Registry::getLang();
            $aLanguages = $oLang->getAllShopLanguageIds();
            $iLang = $this->getLanguage();
            $sLang = $aLanguages[$iLang];

            $oConfig = Registry::getConfig();
            $aMetaVendorTitle = $oConfig->getConfigParam('aarrTRWSeoMetaVendorTitle');
            $aMetaVendorDesc = $oConfig->getConfigParam('aarrTRWSeoMetaVendorDesc');
            $aMetaVendorKeywords = $oConfig->getConfigParam('aarrTRWSeoMetaVendorKeywords');

            $oObject2SeoData = oxNew(Object2SeoData::class);

            if (!$this->getTRWStringData('oxseotitle')) {
                $sDefaultMetaVendorTitle = $aMetaVendorTitle[$sLang];
                $sMetaVendorTitle = str_replace(
                    '%title',
                    $this->getTRWStringData('oxtitle'),
                    $sDefaultMetaVendorTitle
                );

                // create a simple method replaceAdditonalSeoContent in another module
                if (method_exists($this, 'replaceAdditonalSeoContent')) {
                    $sMetaVendorTitle = $this->replaceAdditonalSeoContent($sMetaVendorTitle);
                }

                $this->oxvendor__oxseotitle = new Field($sMetaVendorTitle, Field::T_RAW);
            }

            $bChangeDesc = true;
            $bChangeKeywords = true;
            $oObject2SeoData->loadByObjectId($this->getId(), $iLang);

            if ($oObject2SeoData->isLoaded()) {
                $bChangeDesc = !$oObject2SeoData->getTRWStringData('oxdescription');
                $bChangeKeywords = !$oObject2SeoData->getTRWStringData('oxkeywords');
            }

            if ($bChangeDesc) {
                $sMetaVendorDesc = $this->replaceSeo($aMetaVendorDesc[$sLang] ?: '');

                // create a simple method replaceAdditonalSeoContent in another module
                if (method_exists($this, 'replaceAdditonalSeoContent')) {
                    $sMetaVendorDesc = $this->replaceAdditonalSeoContent($sMetaVendorDesc);
                }
                $oObject2SeoData->oxobject2seodata__oxdescription = new Field($sMetaVendorDesc, Field::T_RAW);
            }

            if ($bChangeKeywords) {
                $sMetaVendorKeywords = $this->replaceSeo($aMetaVendorKeywords[$sLang] ?: '');

                // create a simple method replaceAdditonalSeoContent in another module
                if (method_exists($this, 'replaceAdditonalSeoContent')) {
                    $sMetaVendorKeywords = $this->replaceAdditonalSeoContent($sMetaVendorKeywords);
                }
                $oObject2SeoData->oxobject2seodata__oxkeywords = new Field($sMetaVendorKeywords, Field::T_RAW);
            }

            $oObject2SeoData->save();
        }

        return parent::save();
    }

    /**
     * Ability to control the timing of saving SEO data
     *
     * @param bool $bValue - switch on/off
     */
    public function setSaveSeoData(bool $bValue = true): void
    {
        $this->_bSaveSeoData = $bValue;
    }

    /**
     * Ability to control the timing of saving SEO data
     */
    public function getSaveSeoData(): bool
    {
        return $this->_bSaveSeoData;
    }

    protected function replaceSeo(string $sDefault = ''): string
    {
        return str_replace(
            [
                '%title',
                '%desc'
            ],
            [
                $this->getTRWStringData('oxtitle'),
                Str::getStr()->strip_tags($this->getTrwRawStringData('oxshortdesc')),
            ],
            $sDefault
        );
    }
}
