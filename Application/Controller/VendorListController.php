<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Controller;

/**
 * vendor list class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\VendorListController
 */
class VendorListController extends VendorListController_parent
{
    /** Returns full page title */
    public function getPageTitle(): string
    {
        $sTitle = '';
        if ($oVendor = $this->getActVendor()) {
            $sTitle = $oVendor->getTRWStringData('oxseotitle');
        }
        if (!$sTitle) {
            $sTitle = parent::getPageTitle();
        }

        return $sTitle;
    }
}
