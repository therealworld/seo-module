<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Controller;

use Exception;
use OxidEsales\Eshop\Application\Model\ArticleList;
use OxidEsales\Eshop\Application\Model\CategoryList;
use OxidEsales\Eshop\Application\Model\ContentList;
use OxidEsales\Eshop\Application\Model\ManufacturerList;
use OxidEsales\Eshop\Application\Model\VendorList;
use OxidEsales\Eshop\Core\Model\BaseModel;
use OxidEsales\Eshop\Core\Model\ListModel;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use TheRealWorld\SeoModule\Core\SeoConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

/**
 * Seo Sitemap Generator class.
 */
class SeoSitemapGenerateData
{
    /** The selected Languages */
    protected ?array $_aLanguages = null;

    /** should be created Language Urls */
    protected bool $_bCreateLanguageUrls = false;

    /** should be created Image Urls */
    protected bool $_bCreateImageUrls = false;

    /** should be created Paging Urls */
    protected bool $_bCreatePagingUrls = false;

    /** The default Language */
    protected ?int $_iDefaultLanguage = null;

    /** Nr of Items Per Page */
    protected int $_iNrofItemsPerPage = 0;

    /** The Name of the Sitemap Data Object */
    protected string $_sSitemapObjName = 'trwseositemapexport';

    /** The Object of Sitemap Table */
    protected ?BaseModel $_oSitemapObj = null;

    /**
     * Class constructor, initiates languages.
     */
    public function __construct()
    {
        $oConfig = Registry::getConfig();

        if (is_null($this->_iDefaultLanguage)) {
            $this->_iDefaultLanguage = (int) $oConfig->getConfigParam('sDefaultLang');
        }

        // prepare selected language ids
        if (is_null($this->_aLanguages)) {
            $this->_aLanguages = [];
            $aSelectedLanguages = ToolsConfig::getOptionsArray('arrTRWSeoLanguages');
            if (count($aSelectedLanguages)) {
                $oLang = Registry::getLang();
                $aLangs = $oLang->getLanguageArray();
                foreach ($aLangs as $iId => $oLang) {
                    if ($this->_iDefaultLanguage !== $iId && in_array($oLang->abbr, $aSelectedLanguages, true)) {
                        $this->_aLanguages[] = $iId;
                    }
                }
                if (count($this->_aLanguages)) {
                    $this->_bCreateLanguageUrls = true;
                }
            }
        }

        // should be created imagelinks?
        if ($oConfig->getConfigParam('boolTRWSeoImages')) {
            $this->_bCreateImageUrls = true;
        }

        // should be created paging-links?
        if ($oConfig->getConfigParam('boolTRWSeoCatPageLinks')) {
            $this->_bCreatePagingUrls = true;
            $this->_iNrofItemsPerPage = ToolsConfig::getDefaultNrOfItemsPerPage();
        }
    }

    /**
     * collect the export Data.
     *
     * @param string $sSeoModel - The possible Seo Model
     * @param int    $iStep     - The Step
     *
     * @throws Exception
     */
    public function collectExportData(string $sSeoModel = '', int $iStep = 0): int
    {
        // security check for linkType
        if (!in_array($sSeoModel, SeoConfig::getSeoModels(), true)) {
            return 0;
        }

        $oConfig = Registry::getConfig();

        if (is_null($this->_oSitemapObj)) {
            $this->_oSitemapObj = oxNew(BaseModel::class);
            $this->_oSitemapObj->init($this->_sSitemapObjName);
        }

        $sChangeFrequency = $oConfig->getConfigParam('strTRWSeoChangeFreq' . $sSeoModel);
        $sPriority = $oConfig->getConfigParam('strTRWSeoPriority' . $sSeoModel);

        $aConditions = [['oxissitemap' => 1]];

        // additional SQL Conditions
        if ($sSQLCondition = $oConfig->getConfigParam('strTRWSeoSQLCondition' . $sSeoModel)) {
            $aTmp = explode('|', $sSQLCondition);
            foreach ($aTmp as $sTmp) {
                [$sKey, $sValue] = explode('=', $sTmp);
                if (!empty($sKey) && isset($sValue)) {
                    $sKey = Str::getStr()->strtolower($sKey);
                    $aConditions[] = [
                        'field'     => $sKey,
                        'operator'  => 'and',
                        'condition' => '=',
                        'value'     => $sValue,
                        'noquote'   => false,
                    ];
                }
            }
        }

        if ($oConfig->getConfigParam('boolTRWSeoActive' . $sSeoModel)) {
            $bLinkTypeWithPageUrls = false;

            $sNameTimeStampField = null;
            $oList = null;

            $iExportNrofLines = (int) $oConfig->getConfigParam('numTRWSeoExportNrofLines');

            switch ($sSeoModel) {
                // load articles
                case 'oxarticles':
                    $oList = oxNew(ArticleList::class);
                    $oList->loadAllArticles(
                        $iStep,
                        $iExportNrofLines,
                        false,
                        false,
                        true,
                        [],
                        [],
                        $aConditions
                    );
                    $sNameTimeStampField = 'oxarticles__oxtimestamp';

                    break;

                    // load categories
                case 'oxcategories':
                    $oList = oxNew(CategoryList::class);

                    // fix to not collect external urls
                    $aConditions[] = [
                        'oxextlink' => '',
                    ];

                    $oList->loadAllCategories(
                        $iStep,
                        $iExportNrofLines,
                        true,
                        $aConditions
                    );
                    $sNameTimeStampField = 'oxcategories__oxtimestamp';
                    $bLinkTypeWithPageUrls = true;

                    break;

                    // load contents
                case 'oxcontents':
                    $oList = oxNew(ContentList::class);
                    $oList->loadAllContents(
                        $iStep,
                        $iExportNrofLines,
                        true,
                        $aConditions
                    );
                    $sNameTimeStampField = 'oxcontents__oxtimestamp';

                    break;

                    // load vendors
                case 'oxvendor':
                    $oList = oxNew(VendorList::class);
                    $oList->loadAllVendors(
                        $iStep,
                        $iExportNrofLines,
                        true,
                        $aConditions
                    );
                    $sNameTimeStampField = 'oxvendors__oxtimestamp';

                    break;

                    // load manufacturers
                case 'oxmanufacturers':
                    $oList = oxNew(ManufacturerList::class);
                    $oList->loadAllManufacturers(
                        $iStep,
                        $iExportNrofLines,
                        true,
                        $aConditions
                    );
                    $sNameTimeStampField = 'oxmanufacturers__oxtimestamp';

                    break;
            }

            if (is_null($oList)) {
                $oList = $this->getExternList($sSeoModel, $iStep, $aConditions);
            }

            if (is_null($sNameTimeStampField)) {
                $sNameTimeStampField = $this->getExternNameTimeStampField($sSeoModel);
            }

            if ($oList->count()) {
                $aDefaultParams = [
                    'oxchangefreq'     => $sChangeFrequency,
                    'oxpriority'       => $sPriority,
                    'oxupdateprogress' => 0,
                ];

                foreach ($oList as $oListItem) {
                    $iStep++;

                    $sTimeStamp = $this->getLastModFromMySQLDateTime(
                        $oListItem->getTRWStringData($sNameTimeStampField)
                    );

                    $sOxLoc = $oListItem->getLink($this->_iDefaultLanguage);

                    $aOxLocs = [$sOxLoc];

                    $sObjectId = $oListItem->getId();
                    if ($this->_bCreatePagingUrls && $bLinkTypeWithPageUrls) {
                        $iNrOfSubItems = Registry::getUtilsCount()->getCatArticleCount($sObjectId);
                        $iMaxPageNr = round($iNrOfSubItems / $this->_iNrofItemsPerPage + 0.49);
                        if ($iMaxPageNr > 1) {
                            for ($i = 1; $i <= $iMaxPageNr; $i++) {
                                $aOxLocs[] = Registry::getUtilsUrl()->appendUrl($sOxLoc, ['pgNr' => $i], true);
                            }
                        }
                    }

                    foreach ($aOxLocs as $sOxLoc) {
                        $sOxId = $this->createOxId($sOxLoc);

                        $this->_oSitemapObj->load($sOxId);
                        $aParams = [
                            'oxid'       => $sOxId,
                            'oxparentid' => '',
                            'oxobjectid' => $sObjectId,
                            'oxclass'    => $sSeoModel,
                            'oxtype'     => 'page',
                            'oxlang'     => $this->_iDefaultLanguage,
                            'oxloc'      => $sOxLoc,
                            'oxlastmod'  => $sTimeStamp,
                        ];
                        $aParams += $aDefaultParams;
                        $aParams = ToolsDB::convertDB2OxParams($aParams, $this->_sSitemapObjName);

                        $this->_oSitemapObj->assign($aParams);
                        $this->_oSitemapObj->save();

                        // add alternative Language Urls
                        if ($this->_bCreateLanguageUrls) {
                            $aExtendParams = $aDefaultParams +
                            [
                                'oxclass'    => $sSeoModel,
                                'oxlastmod'  => $sTimeStamp,
                                'oxparentid' => $sOxId,
                                'oxtype'     => 'pagealternative',
                            ];
                            $this->_collectLanguageExportData($oListItem, $aExtendParams);
                        }

                        // add additional Images
                        if ($this->_bCreateImageUrls && in_array($sSeoModel, SeoConfig::getObjectsWithImages(), true)) {
                            $aExtendParams = $aDefaultParams +
                            [
                                'oxclass'    => $sSeoModel,
                                'oxlang'     => $this->_iDefaultLanguage,
                                'oxlastmod'  => $sTimeStamp,
                                'oxparentid' => $sOxId,
                                'oxtype'     => 'image',
                            ];

                            switch ($sSeoModel) {
                                case 'oxarticles':
                                    $this->_collectArticleImageExportData($oListItem, $aExtendParams);

                                    break;

                                default:
                                    $this->_collectImageExportData($oListItem, $aExtendParams, true);
                            }
                        }
                    }
                }
            } else {
                // we have no more data
                $iStep = 0;
            }
        } else {
            // not active, so we close with 0
            $iStep = 0;
        }

        return $iStep;
    }

    /**
     * get Extern ObjList (for overloading).
     *
     * @param string $sSeoModel   - The possible Seo Model
     * @param int    $iStep       - the export step
     * @param array  $aConditions - additional conditions
     */
    public function getExternList(string $sSeoModel, int $iStep, array $aConditions): ?ListModel
    {
        return null;
    }

    /**
     * get Extern NameTimeStampField (for overloading).
     *
     * @param string $sSeoModel - The possible Seo Model
     */
    public function getExternNameTimeStampField(string $sSeoModel): ?string
    {
        return null;
    }

    /** delete all unnecessary data */
    public function cleanExportData(): void
    {
        $sUpdateSql = 'delete
            from ' . $this->_sSitemapObjName . '
            where `oxupdateprogress` = 1
            and `oxshopid` = ' . Registry::getConfig()->getShopId();
        ToolsDB::execute($sUpdateSql);
    }

    /** set the Export Datatable in update-modus */
    public function resetDataTable(): void
    {
        $sUpdateSql = 'update ' . $this->_sSitemapObjName . '
            set `oxupdateprogress` = 1
            and `oxshopid` = ' . Registry::getConfig()->getShopId();
        ToolsDB::execute($sUpdateSql);
    }

    /**
     * collect additional Language Datas.
     *
     * @param object $oItem          - ListItem
     * @param array  $aDefaultParams - the Default Params
     *
     * @throws Exception
     */
    protected function _collectLanguageExportData(object $oItem, array $aDefaultParams): void
    {
        // Link depends on language
        foreach ($this->_aLanguages as $iLangId) {
            $sOxLoc = $oItem->getLink($iLangId);

            $sOxId = $this->createOxId($sOxLoc);
            $this->_oSitemapObj->load($sOxId);

            $aParams = [
                'oxid'   => $sOxId,
                'oxlang' => $iLangId,
                'oxloc'  => $sOxLoc,
            ];

            $aParams += $aDefaultParams;
            $aParams = ToolsDB::convertDB2OxParams($aParams, $this->_sSitemapObjName);
            $this->_oSitemapObj->assign($aParams);
            $this->_oSitemapObj->save();
        }
    }

    /**
     * collect additional Image Datas for Articles.
     *
     * @param $oItem          - ListItem
     * @param $aDefaultParams - the Default Params
     *
     * @throws Exception
     */
    protected function _collectArticleImageExportData(object $oItem, array $aDefaultParams): void
    {
        // Mainpicture
        $aOxLoc = [];

        $sPicture = (string) $oItem->getPictureUrl();

        // filter dummy
        if (stripos($sPicture, 'nopic.jpg')) {
            $aOxLoc[] = $sPicture;
        }

        $aMorePics = $oItem->getPictureGallery();
        $aOxLoc += $aMorePics['Pics'];

        foreach ($aOxLoc as $sOxLoc) {
            $sOxId = $this->createOxId($sOxLoc);
            $this->_oSitemapObj->load($sOxId);

            $aParams = [
                'oxid'  => $sOxId,
                'oxloc' => $sOxLoc,
            ];
            $aParams += $aDefaultParams;
            $aParams = ToolsDB::convertDB2OxParams($aParams, $this->_sSitemapObjName);
            $this->_oSitemapObj->assign($aParams);
            $this->_oSitemapObj->save();
        }
    }

    /**
     * collect additional Image Datas for Categories.
     *
     * @param $oItem          - ListItem
     * @param $aDefaultParams - the Default Params
     * @param $bIconUrl       - is this a IconUrl
     *
     * @throws Exception
     */
    protected function _collectImageExportData(object $oItem, array $aDefaultParams, bool $bIconUrl = false): void
    {
        $sOxLoc = $bIconUrl ? $oItem->getIconUrl() : $oItem->getPictureUrl();

        if (!$sOxLoc) {
            return;
        }

        $sOxId = $this->createOxId($sOxLoc);
        $this->_oSitemapObj->load($sOxId);

        $aParams = [
            'oxid'  => $sOxId,
            'oxloc' => $sOxLoc,
        ];
        $aParams += $aDefaultParams;
        $aParams = ToolsDB::convertDB2OxParams($aParams, $this->_sSitemapObjName);
        $this->_oSitemapObj->assign($aParams);
        $this->_oSitemapObj->save();
    }

    /**
     * create a OxId from a Url.
     *
     * @param string $sUrl - The Url
     */
    protected function createOxId(string $sUrl): string
    {
        $aParsedUrl = parse_url($sUrl);

        $sUrl = $aParsedUrl['scheme']
            . '://' . $aParsedUrl['host']
            . $aParsedUrl['path'] ?? ''
            . $aParsedUrl['query'] ?? '';

        return md5($sUrl);
    }

    /**
     * create a Last Mod Date from MySql DateTime.
     *
     * @param string $sMySqlDateTime - The MySql DateTime
     */
    protected function getLastModFromMySQLDateTime(string $sMySqlDateTime): string
    {
        $sResult = '';
        if ([$sDate] = explode(' ', $sMySqlDateTime)) {
            $sResult = $sDate;
        }

        return $sResult;
    }
}
