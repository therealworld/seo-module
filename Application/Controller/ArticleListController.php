<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Controller;

use OxidEsales\Eshop\Core\Registry;

/**
 * List of articles class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\ArticleListController
 */
class ArticleListController extends ArticleListController_parent
{
    /** Returns full page title */
    public function getPageTitle(): string
    {
        $sTitle = '';
        if ($oCategory = $this->getActiveCategory()) {
            $sTitle = $oCategory->getTRWStringData('oxseotitle');
        } elseif ($oManufacturer = $this->getActManufacturer()) {
            $sTitle = $oManufacturer->getTRWStringData('oxseotitle');
        } elseif ($oVendor = $this->getActVendor()) {
            $sTitle = $oVendor->getTRWStringData('oxseotitle');
        }

        if (!$sTitle) {
            $sTitle = parent::getPageTitle();
        }

        return $sTitle;
    }

    /** Returns view canonical url */
    public function getCanonicalUrl(): string
    {
        $sLink = '';
        if ($oCategory = $this->getActiveCategory()) {
            if (Registry::getUtils()->seoIsActive()) {
                $sLink = $oCategory->getBaseSeoLink($oCategory->getLanguage());
            } else {
                $sLink = $oCategory->getBaseStdLink($oCategory->getLanguage(), $this->getActPage());
            }
        }

        return Registry::getUtilsUrl()->prepareCanonicalUrl($sLink);
    }

    /** Check if noIndex is neccessary */
    public function isNoIndex(): bool
    {
        return $this->getActPage() > 0;
    }
}
