<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Controller;

use Exception;
use OxidEsales\Eshop\Application\Model\ArticleList;
use OxidEsales\Eshop\Application\Model\CategoryList;
use OxidEsales\Eshop\Application\Model\ContentList;
use OxidEsales\Eshop\Application\Model\ManufacturerList;
use OxidEsales\Eshop\Application\Model\VendorList;
use OxidEsales\Eshop\Core\Model\BaseModel;
use OxidEsales\Eshop\Core\Model\ListModel;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\SeoModule\Core\SeoConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

/**
 * Seo Update MetaData Generator class.
 */
class SeoMetaGenerateData
{
    /** The selected Languages */
    protected ?array $_aLanguages = null;

    /** should be created Language Urls */
    protected bool $_bCreateLanguageUrls = false;

    /** should be created Image Urls */
    protected bool $_bCreateImageUrls = false;

    /** should be created Paging Urls */
    protected bool $_bCreatePagingUrls = false;

    /** The default Language */
    protected ?int $_iDefaultLanguage = null;

    /** Nr of Items Per Page */
    protected int $_iNrofItemsPerPage = 0;

    /** The Name of the UpdateMeta Data Object */
    protected string $_sUpdateObjName = 'trwseoupdatemeta';

    /** The Object of Update Meta Table */
    protected ?BaseModel $_oUpdateObj = null;

    /**
     * collect the export Data.
     *
     * @param string $sSeoModel - The possible Seo Model
     * @param int    $iStep     - The Step
     *
     * @throws Exception
     */
    public function collectUpdateData(string $sSeoModel = '', int $iStep = 0): int
    {
        // security check for linkType
        if (!in_array($sSeoModel, SeoConfig::getSeoModels(), true)) {
            return 0;
        }

        $oConfig = Registry::getConfig();

        if (is_null($this->_oUpdateObj)) {
            $this->_oUpdateObj = oxNew(BaseModel::class);
        }

        $aConditions = [];
        $oList = null;

        $iExportNrofLines = (int) $oConfig->getConfigParam('numTRWSeoExportNrofLines');

        switch ($sSeoModel) {
            // load articles
            case 'oxarticles':
                $oList = oxNew(ArticleList::class);
                $oList->loadAllArticles(
                    $iStep,
                    $iExportNrofLines,
                    true,
                    true
                );

                break;

                // load categories
            case 'oxcategories':
                $oList = oxNew(CategoryList::class);

                // fix to not collect external urls
                $aConditions[] = [
                    'oxextlink' => '',
                ];

                $oList->loadAllCategories(
                    $iStep,
                    $iExportNrofLines,
                    true,
                    $aConditions
                );

                break;

                // load contents
            case 'oxcontents':
                $oList = oxNew(ContentList::class);
                $oList->loadAllContents(
                    $iStep,
                    $iExportNrofLines,
                    true,
                    $aConditions
                );

                break;

                // load vendors
            case 'oxvendor':
                $oList = oxNew(VendorList::class);
                $oList->loadAllVendors(
                    $iStep,
                    $iExportNrofLines,
                    true,
                    $aConditions
                );

                break;

                // load manufacturers
            case 'oxmanufacturers':
                $oList = oxNew(ManufacturerList::class);
                $oList->loadAllManufacturers(
                    $iStep,
                    $iExportNrofLines,
                    true,
                    $aConditions
                );

                break;
        }

        if (is_null($oList)) {
            $oList = $this->getExternList($sSeoModel, $iStep, $aConditions);
        }

        if ($oList->count()) {
            $className = null;

            $aDefaultParams = [
                'oxupdateprogress' => 0,
            ];

            foreach ($oList as $oListItem) {
                if (is_null($className) && $oListItem) {
                    $className = get_class($oListItem);
                    // actual class, might be shop class chain extended by module
                    while (
                        $className
                        && (stripos(get_parent_class($className), 'multilanguagemodel') === false)
                        && (stripos(get_parent_class($className), 'basemodel') === false)
                        && (stripos($className, 'OxidEsales\EShop\Application\Model') === false)
                    ) {
                        $className = get_parent_class($className);
                    }
                }
                $iStep++;

                $sOxId = Registry::getUtilsObject()->generateUID();

                $this->_oUpdateObj->init($this->_sUpdateObjName);
                $aParams = [
                    'oxid'                 => $sOxId,
                    'oxobjectid'           => $oListItem->getId(),
                    'oxclass'              => $sSeoModel,
                    'oxnamespaceclassname' => $className,
                ];
                $aParams += $aDefaultParams;
                $aParams = ToolsDB::convertDB2OxParams($aParams, $this->_sUpdateObjName);

                $this->_oUpdateObj->assign($aParams);
                $this->_oUpdateObj->save();
            }
        } else {
            // we have no more data
            $iStep = 0;
        }

        return $iStep;
    }

    /**
     * get Extern ObjList (for overloading).
     *
     * @param string $sSeoModel   - The possible Seo Model
     * @param int    $iStep       - the export step
     * @param array  $aConditions - additional conditions
     */
    public function getExternList(string $sSeoModel, int $iStep, array $aConditions): ?ListModel
    {
        return null;
    }

    /** delete all unnecessary data */
    public function cleanUpdateData(): void
    {
        $sUpdateSql = 'delete
            from ' . $this->_sUpdateObjName . '
            where `oxupdateprogress` = 1';
        ToolsDB::execute($sUpdateSql);
    }

    /** set the Export Datatable in update-modus */
    public function resetDataTable(): void
    {
        $sUpdateSql = 'update ' . $this->_sUpdateObjName . '
            set `oxupdateprogress` = 1';
        ToolsDB::execute($sUpdateSql);
    }
}
