<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Model\Category;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

/**
 * Category seo config class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\Admin\CategorySeo
 */
class CategorySeo extends CategorySeo_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function render(): string
    {
        parent::render();

        $this->_aViewData['isEnabledForSeo'] = true;

        return 'object_seo.tpl';
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function save()
    {
        parent::save();

        $sOxId = $this->getEditObjectId();
        $oRequest = Registry::getRequest();
        $aTplParams = $oRequest->getRequestParameter('aSeoData');
        $blShowSuffixParameter = $oRequest->getRequestParameter('blShowSuffix');

        $oCategory = oxNew(Category::class);
        $oCategory->setLanguage($this->_iEditLang);

        if ($sOxId !== '-1') {
            $oCategory->loadInLang($this->_iEditLang, $sOxId);
        }
        $aParams = [
            'oxshowsuffix' => $blShowSuffixParameter,
            'oxissitemap'  => ($aTplParams['oxissitemap'] ? 1 : 0),
            'oxseotitle'   => $aTplParams['oxseotitle'],
        ];
        $aParams = ToolsDB::convertDB2OxParams($aParams, 'oxcategories');
        $oCategory->assign($aParams);
        $oCategory->save();
    }

    /**
     * Template Getter, is Sitemap active for Category.
     */
    public function isSitemap(): bool
    {
        $oCategory = oxNew(Category::class);
        $oCategory->load($this->getEditObjectId());

        return $oCategory->getTRWBoolData('oxissitemap');
    }

    /**
     * Template Getter, Seo-Title.
     */
    public function getSeoTitle(): string
    {
        $oCategory = oxNew(Category::class);
        $oCategory->loadInLang($this->_iEditLang, $this->getEditObjectId());

        return $oCategory->getTRWStringData('oxseotitle');
    }
}
