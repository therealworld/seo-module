<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Controller\Admin;

use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;

/**
 * ModuleConfiguration class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\Admin\ModuleConfiguration
 */
class ModuleConfiguration extends ModuleConfiguration_parent
{
    /**
     * Template Getter. List of additional Seo Languages.
     */
    public function getAdditionalSeoLanguages(): array
    {
        $aResult = [];

        $iDefaultLanguage = (int) Registry::getConfig()->getConfigParam('sDefaultLang');

        // Language List
        $aSelectedLanguages = ToolsConfig::getOptionsArray('arrTRWSeoLanguages');
        $oLang = Registry::getLang();
        $aLangs = $oLang->getLanguageArray();
        foreach ($aLangs as $iId => $oLang) {
            if ($iDefaultLanguage !== $iId) {
                $oLang->selected = in_array($oLang->abbr, $aSelectedLanguages, true);
                $aResult[$iId] = clone $oLang;
            }
        }

        return $aResult;
    }
}
