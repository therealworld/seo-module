<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Model\Vendor;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

/**
 * Vendor seo config class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\Admin\VendorSeo
 */
class VendorSeo extends VendorSeo_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function render(): string
    {
        parent::render();

        $this->_aViewData['isEnabledForSeo'] = true;

        return 'object_seo.tpl';
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function save(): void
    {
        parent::save();

        $sOxId = $this->getEditObjectId();
        $oRequest = Registry::getRequest();
        $aTplParams = $oRequest->getRequestParameter('aSeoData');
        $blShowSuffixParameter = $oRequest->getRequestParameter('blShowSuffix');

        $oVendor = oxNew(Vendor::class);
        $oVendor->setLanguage($this->_iEditLang);

        if ($sOxId !== '-1') {
            $oVendor->loadInLang($this->_iEditLang, $sOxId);
        }
        $aParams = [
            'oxshowsuffix' => $blShowSuffixParameter,
            'oxissitemap'  => ($aTplParams['oxissitemap'] ? 1 : 0),
            'oxseotitle'   => $aTplParams['oxseotitle'],
        ];
        $aParams = ToolsDB::convertDB2OxParams($aParams, 'oxvendor');
        $oVendor->assign($aParams);
        $oVendor->save();
    }

    /**
     * Template Getter, is Sitemap active for Vendor.
     */
    public function isSitemap(): bool
    {
        $oVendor = oxNew(Vendor::class);
        $oVendor->load($this->getEditObjectId());

        return $oVendor->getTRWBoolData('oxissitemap');
    }

    /**
     * Template Getter, Seo-Title.
     */
    public function getSeoTitle(): string
    {
        $oVendor = oxNew(Vendor::class);
        $oVendor->loadInLang($this->_iEditLang, $this->getEditObjectId());

        return $oVendor->getTRWStringData('oxseotitle');
    }
}
