<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Model\Manufacturer;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

/**
 * Manufacturer seo config class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\Admin\ManufacturerSeo
 */
class ManufacturerSeo extends ManufacturerSeo_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function render(): string
    {
        parent::render();

        $this->_aViewData['isEnabledForSeo'] = true;

        return 'object_seo.tpl';
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function save(): void
    {
        parent::save();

        $sOxId = $this->getEditObjectId();
        $oRequest = Registry::getRequest();
        $aTplParams = $oRequest->getRequestParameter('aSeoData');
        $blShowSuffixParameter = $oRequest->getRequestParameter('blShowSuffix');

        $oManufacturer = oxNew(Manufacturer::class);
        $oManufacturer->setLanguage($this->_iEditLang);

        if ($sOxId !== '-1') {
            $oManufacturer->loadInLang($this->_iEditLang, $sOxId);
        }
        $aParams = [
            'oxshowsuffix' => $blShowSuffixParameter,
            'oxissitemap'  => ($aTplParams['oxissitemap'] ? 1 : 0),
            'oxseotitle'   => $aTplParams['oxseotitle'],
        ];
        $aParams = ToolsDB::convertDB2OxParams($aParams, 'oxmanufacturers');
        $oManufacturer->assign($aParams);
        $oManufacturer->save();
    }

    /**
     * Template Getter, is Sitemap active for Manufacturer.
     */
    public function isSitemap(): bool
    {
        $oManufacturer = oxNew(Manufacturer::class);
        $oManufacturer->load($this->getEditObjectId());

        return $oManufacturer->getTRWBoolData('oxissitemap');
    }

    /**
     * Template Getter, Seo-Title.
     */
    public function getSeoTitle(): string
    {
        $oManufacturer = oxNew(Manufacturer::class);
        $oManufacturer->loadInLang($this->_iEditLang, $this->getEditObjectId());

        return $oManufacturer->getTRWStringData('oxseotitle');
    }
}
