<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Model\Content;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

/**
 * Content seo config class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\Admin\ContentSeo
 */
class ContentSeo extends ContentSeo_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function render(): string
    {
        parent::render();

        $this->_aViewData['isEnabledForSeo'] = true;

        return 'object_seo.tpl';
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function save()
    {
        parent::save();

        $sOxId = $this->getEditObjectId();
        $oRequest = Registry::getRequest();
        $aTplParams = $oRequest->getRequestParameter('aSeoData');
        $blShowSuffixParameter = $oRequest->getRequestParameter('blShowSuffix');

        $oContent = oxNew(Content::class);
        $oContent->setLanguage($this->_iEditLang);

        if ($sOxId !== '-1') {
            $oContent->loadInLang($this->_iEditLang, $sOxId);
        }
        $aParams = [
            'oxshowsuffix' => $blShowSuffixParameter,
            'oxissitemap'  => ($aTplParams['oxissitemap'] ? 1 : 0),
            'oxseotitle'   => $aTplParams['oxseotitle'],
        ];
        $aParams = ToolsDB::convertDB2OxParams($aParams, 'oxcontents');
        $oContent->assign($aParams);
        $oContent->save();
    }

    /**
     * Template Getter, is Sitemap active for Content.
     */
    public function isSitemap(): bool
    {
        $oContent = oxNew(Content::class);
        $oContent->load($this->getEditObjectId());

        return $oContent->getTRWBoolData('oxissitemap');
    }

    /**
     * Template Getter, Seo-Title.
     */
    public function getSeoTitle(): string
    {
        $oContent = oxNew(Content::class);
        $oContent->loadInLang($this->_iEditLang, $this->getEditObjectId());

        return $oContent->getTRWStringData('oxseotitle');
    }
}
