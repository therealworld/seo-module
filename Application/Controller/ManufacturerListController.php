<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Controller;

/**
 * manufacturer list class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\ManufacturerListController
 */
class ManufacturerListController extends ManufacturerListController_parent
{
    /** Returns full page title */
    public function getPageTitle(): string
    {
        $sTitle = '';
        if ($oManufacturer = $this->getActManufacturer()) {
            $sTitle = $oManufacturer->getTRWStringData('oxseotitle');
        }
        if (!$sTitle) {
            $sTitle = parent::getPageTitle();
        }

        return $sTitle;
    }
}
