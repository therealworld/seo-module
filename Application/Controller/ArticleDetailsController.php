<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\Application\Controller;

/**
 * article details class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\ArticleDetailsController
 */
class ArticleDetailsController extends ArticleDetailsController_parent
{
    /** Returns full page title */
    public function getPageTitle(): string
    {
        $sTitle = '';
        if ($oProduct = $this->getProduct()) {
            $sTitle = $oProduct->getTRWStringData('oxseotitle');
        }
        if (!$sTitle) {
            $sTitle = parent::getPageTitle();
        }

        return $sTitle;
    }
}
