<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'TASKSEOSITEMAPEXPORT' => 'sitemap.xml Export',
    'TASKSEOUPDATEMETA'    => 'Update Metadata',

    'EXPORTNOTPOSSIBLE'   => 'not possible',
    'DELIVERYTIME'        => 'Deliverytime',
    'DELIVERYTIME_DAY'    => 'Day',
    'DELIVERYTIME_DAYS'   => 'Days',
    'DELIVERYTIME_MONTH'  => 'Month',
    'DELIVERYTIME_MONTHS' => 'Months',
    'DELIVERYTIME_WEEK'   => 'Week',
    'DELIVERYTIME_WEEKS'  => 'Weeks',
    'WRITEXMLPART_ERROR'  => 'No sitemap%s.xml created! The path %s may not be writable.',
    'WRITEXMLFINAL_ERROR' => 'No sitemap.xml%s created! Please check the settings.',
    'WRITEXML_SUCCESS'    => 'sitemap.xml%s created. The index-sitemap contains %s reference(s) to additional sitemap files.',
    'COLLECT_DATA_MODEL'  => 'Data collected for type %s.',
    'UPDATED_DATA_MODEL'  => '%s Data processed on stack',
];
