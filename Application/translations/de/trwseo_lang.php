<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'TASKSEOSITEMAPEXPORT' => 'sitemap.xml Export',
    'TASKSEOUPDATEMETA'    => 'Update Metadaten',

    'EXPORTNOTPOSSIBLE'   => 'nicht möglich',
    'DELIVERYTIME'        => 'Lieferzeit',
    'DELIVERYTIME_DAY'    => 'Tag',
    'DELIVERYTIME_DAYS'   => 'Tage',
    'DELIVERYTIME_MONTH'  => 'Monat',
    'DELIVERYTIME_MONTHS' => 'Monate',
    'DELIVERYTIME_WEEK'   => 'Woche',
    'DELIVERYTIME_WEEKS'  => 'Wochen',
    'WRITEXMLPART_ERROR'  => 'Keine sitemap%s.xml erstellt! Mglw. ist der Pfad %s nicht beschreibar.',
    'WRITEXMLFINAL_ERROR' => 'Keine sitemap.xml%s erstellt! Bitte Einstellungen prüfen.',
    'WRITEXML_SUCCESS'    => 'sitemap.xml%s erstellt. Die index-sitemap enthält %s Verweis(e) auf weitere sitemap-Dateien.',
    'COLLECT_DATA_MODEL'  => 'Daten für Typ %s gesammelt.',
    'UPDATED_DATA_MODEL'  => '%s Daten im Stapel verarbeitet',
];
