<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\SchedulerTasks;

use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\SchedulerModule\Core\ISchedulerTask;
use TheRealWorld\SeoModule\Application\Controller\SeoMetaGenerateData;
use TheRealWorld\SeoModule\Application\Model\Object2SeoData;
use TheRealWorld\SeoModule\Application\Model\SeoUpdateMetaList;
use TheRealWorld\SeoModule\Core\SeoConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;
use TheRealWorld\ToolsPlugin\Core\ToolsLog;

/**
 * SeoMetaData Update Task Class.
 */
class TaskSeoUpdateMeta implements ISchedulerTask
{
    /** String of a unix-style crontab */
    protected string $_sDefaultCrontab = ''; // we have no default crontab, so the task can only run manually

    /** Number of the next step */
    protected int $_iNextStep = 0;

    /** Number of the next sub step */
    protected int $_iNextSubStep = 0;

    /** Script Stoptime */
    protected ?int $_iStopTime = null;

    /** The Object for Prepare Data */
    protected ?object $_oGenerateData = null;

    /** Returns the default crontab of the Task */
    public function getDefaultCrontab(): string
    {
        return $this->_sDefaultCrontab;
    }

    /**
     * get Path for "manual" Files.
     *
     * @param string $sPathKey - a possible key for several paths
     */
    public function getPathForManualFiles(string $sPathKey = ''): string
    {
        return '';
    }

    /**
     * get List of "manual" Files.
     *
     * @param string $sPathKey - a possible key for several paths
     */
    public function getManualFileList(string $sPathKey = ''): array
    {
        return [];
    }

    /** Install the Task */
    public function install(): bool
    {
        return true;
    }

    /**
     * Run the Task.
     *
     * @param int  $iCurrentStep    - The number of the current step
     * @param int  $iCurrentSupStep - The number of the current sub step
     * @param bool $bRunManually    - run the task manually?
     */
    public function run(int $iCurrentStep = 0, int $iCurrentSupStep = 0, bool $bRunManually = false): bool
    {
        $this->_iNextStep = $iCurrentStep;
        $this->_iNextSubStep = $iCurrentSupStep;
        $this->_iStopTime = Registry::getSession()->getVariable('iTRWSchedulerStopTimeStamp');

        $result = false;

        if (is_null($this->_oGenerateData)) {
            $this->_oGenerateData = oxNew(SeoMetaGenerateData::class);
        }

        // count the number of Seo Models
        // the number of Seo Models corresponding to the number of "_generateData" steps
        $iNrOfSeoModels = count(SeoConfig::getSeoModels());

        // Only in the beginning we reset the data table
        if ($iCurrentStep === 0 && $iCurrentSupStep === 0) {
            $this->_resetData();
        }

        // First collect the export Data
        if ($this->_iNextStep < $iNrOfSeoModels) {
            $result = $this->_generateData();
        }

        // Second export the Data to XML
        if ($this->_iNextStep >= $iNrOfSeoModels) {
            $result = $this->_updateData();
        }

        if ($this->_iNextStep === 0 && $this->_iNextSubStep === 0) {
            $this->_cleanData();
        }

        return $result;
    }

    /**
     * Get the number of the next step.
     *
     * @param bool $bSub - The Sub-Step?
     */
    public function getNextStep(bool $bSub = false): int
    {
        return $bSub ? $this->_iNextSubStep : $this->_iNextStep;
    }

    /** generate the Data */
    protected function _generateData(): bool
    {
        $iCountLinkType = 0;

        // goes through all Link types
        foreach (SeoConfig::getSeoModels() as $sSeoModel) {
            // when we start after a break, so continue by the right Type
            if ($iCountLinkType < $this->_iNextStep) {
                $iCountLinkType++;

                continue;
            }

            do {
                $this->_iNextSubStep = $this->_oGenerateData->collectUpdateData($sSeoModel, $this->_iNextSubStep);
            } while (($bCheckTime = (time() < $this->_iStopTime)) && $this->_iNextSubStep !== 0);

            // if we are just in time and we have nothing substeps
            // we got to the next step
            if ($bCheckTime && $this->_iNextSubStep === 0) {
                $this->_iNextStep++;
            } else {
                break;
            }
            $iCountLinkType++;
            ToolsLog::setLogEntry(
                sprintf(
                    Registry::getLang()->translateString('COLLECT_DATA_MODEL'),
                    (string) $sSeoModel,
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'info'
            );
        }

        return true;
    }

    /** update the Data */
    protected function _updateData(): bool
    {
        $oConfig = Registry::getConfig();
        $oLang = Registry::getLang();
        $aLanguages = $oLang->getAllShopLanguageIds();

        $oSeoUpdateMetaList = oxNew(SeoUpdateMetaList::class);

        do {
            $iStep = 0;
            $oSeoUpdateMetaList->loadUpdateMetaList(
                $this->_iNextSubStep,
                (int) $oConfig->getConfigParam('numTRWSeoExportNrofLines')
            );
            if ($iUpdateCount = $oSeoUpdateMetaList->count()) {
                foreach ($oSeoUpdateMetaList as $oDataListItem) {
                    $sDataClass = $oDataListItem->getTRWStringData('oxclass');
                    $sDataClassNameSpaceName = $oDataListItem->getTRWStringData('oxnamespaceclassname');
                    $sObjId = $oDataListItem->getTRWStringData('oxobjectid');
                    $oDataObj = oxNew($sDataClassNameSpaceName);
                    $oObject2SeoData = oxNew(Object2SeoData::class);
                    foreach ($aLanguages as $iLang => $sLang) {
                        $oDataObj->loadInLang($iLang, $sObjId);
                        if ($oConfig->getConfigParam('boolTRWSeoMetaDeleteBeforeUpdate')) {
                            // title
                            $aParams = [
                                'oxseotitle' => ''
                            ];
                            $aParams = ToolsDB::convertDB2OxParams($aParams, $sDataClass);
                            $oDataObj->assign($aParams);
                            // description & keywords
                            $oObject2SeoData->loadByObjectId($sObjId, $iLang);
                            $aParams = [
                                'oxkeywords'    => '',
                                'oxdescription' => '',
                            ];
                            $aParams = ToolsDB::convertDB2OxParams($aParams, 'oxobject2seodata');
                            $oObject2SeoData->assign($aParams);
                            $oObject2SeoData->save();
                        }
                        $oDataObj->save();
                    }
                    $iStep++;

                    $oDataListItem->trwseoupdatemeta__oxupdateprogress = new Field('1', Field::T_RAW);
                    $oDataListItem->save();
                }
            }

            ToolsLog::setLogEntry(
                sprintf(
                    $oLang->translateString('UPDATED_DATA_MODEL'),
                    $iUpdateCount
                ),
                __CLASS__ . ' - ' . __FUNCTION__
            );

            $this->_iNextSubStep = $iStep ? $this->_iNextSubStep + $iStep : 0;
        } while (
            time() < $this->_iStopTime
            && $this->_iNextSubStep !== 0
        );

        if ($this->_iNextSubStep === 0) {
            $this->_iNextStep = 0;
        }

        return true;
    }

    /** clean the Data Table */
    protected function _cleanData(): void
    {
        $this->_oGenerateData->cleanUpdateData();
    }

    /** reset the Data Table */
    protected function _resetData(): void
    {
        $this->_oGenerateData->resetDataTable();
    }
}
