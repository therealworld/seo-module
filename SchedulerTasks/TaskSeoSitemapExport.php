<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SeoModule\SchedulerTasks;

use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\SchedulerModule\Core\ISchedulerTask;
use TheRealWorld\SeoModule\Application\Controller\SeoSitemapGenerateData;
use TheRealWorld\SeoModule\Application\Model\SeoSitemapExport;
use TheRealWorld\SeoModule\Application\Model\SeoSitemapExportList;
use TheRealWorld\SeoModule\Core\SeoConfig;
use TheRealWorld\ToolsModule\Application\Controller\ToolsExportXML;
use TheRealWorld\ToolsPlugin\Core\ToolsFile;
use TheRealWorld\ToolsPlugin\Core\ToolsLog;
use TheRealWorld\ToolsPlugin\Core\ToolsString;
use TheRealWorld\ToolsPlugin\Core\ToolsXML;

/**
 * SeoSitemap Export Task Class.
 */
class TaskSeoSitemapExport implements ISchedulerTask
{
    /** String of a unix-style crontab */
    protected string $_sDefaultCrontab = '10 2 * * *'; // Run at 2:10 am

    /** Number of the next step */
    protected int $_iNextStep = 0;

    /** Number of the next sub step */
    protected int $_iNextSubStep = 0;

    /** Script Stoptime */
    protected ?int $_iStopTime = null;

    /** The Object for Export a XML */
    protected ?object $_oExportXML = null;

    /** The Object for Prepare Data */
    protected ?object $_oGenerateData = null;

    /** Returns the default crontab of the Task */
    public function getDefaultCrontab(): string
    {
        return $this->_sDefaultCrontab;
    }

    /**
     * get Path for "manual" Files.
     *
     * @param string $sPathKey - a possible key for several paths
     */
    public function getPathForManualFiles(string $sPathKey = ''): string
    {
        return '';
    }

    /**
     * get List of "manual" Files.
     *
     * @param string $sPathKey - a possible key for several paths
     */
    public function getManualFileList(string $sPathKey = ''): array
    {
        return [];
    }

    /** Install the Task */
    public function install(): bool
    {
        return true;
    }

    /**
     * Run the Task.
     *
     * @param int  $iCurrentStep    - The number of the current step
     * @param int  $iCurrentSupStep - The number of the current sub step
     * @param bool $bRunManually    - run the task manually?
     */
    public function run(int $iCurrentStep = 0, int $iCurrentSupStep = 0, bool $bRunManually = false): bool
    {
        $this->_iNextStep = $iCurrentStep;
        $this->_iNextSubStep = $iCurrentSupStep;
        $this->_iStopTime = Registry::getSession()->getVariable('iTRWSchedulerStopTimeStamp');

        $result = false;

        if (is_null($this->_oGenerateData)) {
            $this->_oGenerateData = oxNew(SeoSitemapGenerateData::class);
        }

        // count the number of Seo Models
        // the number of Seo Models corresponding to the number of "_generateData" steps
        $iNrOfSeoModels = count(SeoConfig::getSeoModels());

        // Only in the beginning we reset the data table
        if ($iCurrentStep === 0 && $iCurrentSupStep === 0) {
            $this->_resetData();
        }

        // First collect the export Data
        if ($this->_iNextStep < $iNrOfSeoModels) {
            $result = $this->_generateData();
        }

        // Second export the Data to XML
        if ($this->_iNextStep >= $iNrOfSeoModels) {
            $result = $this->_exportData();
        }

        if ($this->_iNextStep === 0 && $this->_iNextSubStep === 0) {
            $this->_cleanData();
        }

        return $result;
    }

    /**
     * Get the number of the next step.
     *
     * @param bool $bSub - The Sub-Step?
     */
    public function getNextStep(bool $bSub = false): int
    {
        return $bSub ? $this->_iNextSubStep : $this->_iNextStep;
    }

    /** generate the Data */
    protected function _generateData(): bool
    {
        $iCountLinkType = 0;

        // goes through all Link types
        foreach (SeoConfig::getSeoModels() as $sSeoModel) {
            // when we start after a break, so continue by the right Type
            if ($iCountLinkType < $this->_iNextStep) {
                $iCountLinkType++;

                continue;
            }

            do {
                $this->_iNextSubStep = $this->_oGenerateData->collectExportData($sSeoModel, $this->_iNextSubStep);
            } while (($bCheckTime = (time() < $this->_iStopTime)) && $this->_iNextSubStep !== 0);

            // if we are just in time and we have nothing substeps
            // we got to the next step
            if ($bCheckTime && $this->_iNextSubStep === 0) {
                $this->_iNextStep++;
            } else {
                break;
            }
            $iCountLinkType++;
            ToolsLog::setLogEntry(
                sprintf(
                    Registry::getLang()->translateString('COLLECT_DATA_MODEL'),
                    (string) $sSeoModel,
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'info'
            );
        }

        return true;
    }

    /** export the Data */
    protected function _exportData(): bool
    {
        $oConfig = Registry::getConfig();
        $oLang = Registry::getLang();

        $sSitemapPathRelative = SeoConfig::getSitemapFilePath();
        $sSiteMapPathAbsolute = SeoConfig::getSitemapFilePath(false);
        $sSiteMapUrl = $oConfig->getShopUrl() . $sSitemapPathRelative;

        $bCompress = $oConfig->getConfigParam('boolTRWSeoCompressExport');

        if (is_null($this->_oExportXML)) {
            // check if we must change the file
            $sFileName = SeoConfig::getSitemapFileName(
                true,
                $this->_iNextSubStep === 0
            );

            $this->_oExportXML = oxNew(ToolsExportXML::class);
            $this->_oExportXML->setOptions([
                '_sExportPath'             => $sSitemapPathRelative,
                '_sFileName'               => $sFileName,
                '_sXMLStartNode'           => 'urlset',
                '_aXMLStartNodeAttributes' => SeoConfig::getStartNodeAttributes(),
            ]);

            $this->_oExportXML->initXML();
        }

        $oSeoSitemapExportList = oxNew(SeoSitemapExportList::class);

        do {
            $iStep = 0;
            $oSeoSitemapExportList->loadSitemapExportList(
                $this->_iNextSubStep,
                (int) $oConfig->getConfigParam('numTRWSeoExportNrofLines')
            );
            if ($oSeoSitemapExportList->count()) {
                $aData = [];
                foreach ($oSeoSitemapExportList as $oDataListItem) {
                    $aTmp = [
                        'loc'        => ToolsString::encodeUtf8($oDataListItem->getTRWStringData('oxloc')),
                        'lastmod'    => ToolsString::encodeUtf8($oDataListItem->getTRWStringData('oxlastmod')),
                        'changefreq' => ToolsString::encodeUtf8($oDataListItem->getTRWStringData('oxchangefreq')),
                        'priority'   => ToolsString::encodeUtf8($oDataListItem->getTRWStringData('oxpriority')),
                    ];

                    if (method_exists($oDataListItem, 'getAdditionalData')) {
                        // @var SeoSitemapExport $oDataListItem
                        $aTmp += $oDataListItem->getAdditionalData();
                    }

                    $oDataListItem->trwseositemapexport__oxupdateprogress = new Field('1', Field::T_RAW);
                    $oDataListItem->save();

                    $iStep++;

                    $aData['url'][] = $aTmp;
                }
                $bResult = $this->_oExportXML->writeExportData($aData, $this->_iNextSubStep === 0);
                if ($bResult === false) {
                    ToolsLog::setLogEntry(
                        sprintf(
                            $oLang->translateString('WRITEXMLPART_ERROR'),
                            (string) SeoConfig::getSitemapFileNumber(),
                            $sSiteMapPathAbsolute
                        ),
                        __CLASS__ . ' - ' . __FUNCTION__,
                        'error'
                    );
                }
            }
            if ($iStep === 0) {
                // close XML
                $this->_oExportXML->closeXML();
            }

            $this->_iNextSubStep = $iStep ? $this->_iNextSubStep + $iStep : 0;
        } while (
            time() < $this->_iStopTime
            && $this->_iNextSubStep !== 0
            && !SeoConfig::isFileChangeNecessary($this->_iNextSubStep)
        );

        if ($this->_iNextSubStep === 0) {
            $this->_iNextStep = 0;

            // write the index-sitemap
            $sCompress = ($bCompress ? '.gz' : '');
            $aSitemapFiles = [];

            for ($iCounter = 1; $iCounter <= SeoConfig::getSitemapFileNumber(); $iCounter++) {
                $sSiteMapFileName = SeoConfig::getSitemapFileName(false, false, $iCounter);
                if (file_exists($sSiteMapPathAbsolute . $sSiteMapFileName)) {
                    $sSiteMapFileUrl = $sSiteMapUrl . $sSiteMapFileName;
                    $aSitemapFiles['sitemap'][] = [
                        'loc'     => $sSiteMapFileUrl . $sCompress,
                        'lastmod' => date('c', filemtime($sSiteMapPathAbsolute . $sSiteMapFileName)),
                    ];
                    // compress the file
                    if ($bCompress) {
                        ToolsFile::compressGZFile($sSiteMapFileName, $sSiteMapPathAbsolute);
                    }
                }
            }

            if ($iCountSitemapFiles = count($aSitemapFiles)) {
                $sSiteMapFileName = SeoConfig::getSitemapIndexFileName();

                ToolsXML::getXMLFileFromArray(
                    $sSiteMapFileName,
                    $sSitemapPathRelative,
                    $aSitemapFiles,
                    'sitemapindex',
                    SeoConfig::getStartNodeAttributes(),
                    false
                );
                // compress the file
                if ($bCompress) {
                    ToolsFile::compressGZFile($sSiteMapFileName, $sSiteMapPathAbsolute);
                }
                ToolsLog::setLogEntry(
                    sprintf(
                        $oLang->translateString('WRITEXML_SUCCESS'),
                        $sCompress,
                        $iCountSitemapFiles
                    ),
                    __CLASS__ . ' - ' . __FUNCTION__
                );
            } else {
                ToolsLog::setLogEntry(
                    sprintf(
                        $oLang->translateString('WRITEXMLFINAL_ERROR'),
                        $sCompress
                    ),
                    __CLASS__ . ' - ' . __FUNCTION__,
                    'error'
                );
            }
        } else {
            // close the actual XML-File
            $this->_oExportXML->closeXML();
        }

        return true;
    }

    /** clean the Data Table */
    protected function _cleanData(): void
    {
        $this->_oGenerateData->cleanExportData();
    }

    /** reset the Data Table */
    protected function _resetData(): void
    {
        $this->_oGenerateData->resetDataTable();
    }
}
